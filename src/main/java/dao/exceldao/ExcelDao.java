package dao.exceldao;

import java.util.List;

import InputExcel.Achievement;

public interface ExcelDao {
	//excel导出到mysql中 传入excel地址
	boolean importMySQL(String File_Data);
	//excel导出到list中
	List<Achievement> outExcel(String data_file);
	//excel导出到json中
	void outExcel(String data_file,String json_file);
	

	//创建excel(存在就增加，不存在创建),由list列表导入
	void creatExcel(String data_file,List<Achievement> list);
	//导入 由MySQL导入到excel
	void mySQLToExcel(String data_file);
	//导入 由Json导入到excel
	void jsonToExcel(String json_file,String data_file);
}

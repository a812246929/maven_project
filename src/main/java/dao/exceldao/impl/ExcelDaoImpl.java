package dao.exceldao.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.alibaba.fastjson.JSON;

import InputExcel.Achievement;
import dao.demo.ExcelToMySQLImpl;
import dao.exceldao.ExcelDao;
import dao.mysqldao.MySQLDao;
import dao.mysqldao.impl.MySQLDaoImpl;

public class ExcelDaoImpl implements ExcelDao {
	protected Logger logger = LogManager.getLogger(this.getClass().getName());

	String url = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
	String user = "root";
	String password = "lilin123";

	Connection conn = null;
	ResultSet rs = null;
	Statement st = null;
	PreparedStatement pst = null;

	public ExcelDaoImpl() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url, user, password);
			st = conn.createStatement();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean importMySQL(String File_Data) {
		boolean flag = false;
		List<Achievement> list = new ArrayList<>();
		list = outExcel(File_Data);
		String s = "insert into t_student(ID,CODE,NAME,LISAN) values(?,?,?,?)";
		try {
			for (Achievement achievement : list) {
				pst = conn.prepareStatement(s);
				pst.setString(1, achievement.getAdminClass());
				pst.setString(2, achievement.getCode());
				pst.setString(3, achievement.getName());
				pst.setString(4, achievement.getGrade());
				pst.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return flag;
		} finally {
			try {
				if(pst != null) {
					pst.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				if (logger.isDebugEnabled()) {
					logger.debug("无法结束"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				logger.error(e); // $NON-NLS-1$
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("导入数据库完成"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		flag = true;
		return flag;
	}
	
	@Override
	public List<Achievement> outExcel(String data_file) {
		List<Achievement> studentList = new ArrayList<Achievement>();
		/**
		 * 读取Excel
		 */
		InputStream input = ExcelToMySQLImpl.class.getResourceAsStream(data_file);
		// 创建文件对象wb
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(input);
		} catch (IOException e) {
			logger.error(e); // $NON-NLS-1$
		}
		// excel中的sheet页
		XSSFSheet sheet = wb.getSheetAt(0);
		// 处理一页中的每一行 从第二行开始 第一行就是0，第二行就是1
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			// row 一排 处理一排数据
			Row row = sheet.getRow(i);
			Achievement s = new Achievement();
			/**
			 * Set the system unique Id for object
			 */
			// 设置编号
			s.setId(Long.valueOf(1000 + i));
			for (int j = 0; j < row.getLastCellNum(); j++) {
				// cell 小格格 获得小格格数据
				Cell cell = row.getCell(j);
				if (cell != null) {
					cell.setCellType(CellType.STRING);
					String value = row.getCell(j).getStringCellValue().trim();
					if (j == 1)
						s.setCode(value);
					if (j == 2)
						s.setName(value);
					if (j == 0)
						s.setAdminClass(value);
					if (j == 3)
						s.setGrade(value);
				}
			}
			studentList.add(s);
		}
		return studentList;
	}
	
	//创建excel 文件存在就添加文件
	@Override
	public void creatExcel(String data_file,List<Achievement> list) {
		XSSFWorkbook wb = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		File file = new File(data_file);
		if (file.exists()) {
			//存在 即在最后导入
			//判断是否为excel类型文件
			if (!data_file.endsWith(".xlsx")) {
				System.out.println("该文件不是excel类型");
				return;
			}
			System.out.println("文件存在");
			try {
				fis = new FileInputStream(file);
				wb = new XSSFWorkbook(fis);
				// 获取sheet
				// 获取最后一行row，创建row，添加值，可以设置单元格类型，也可以不设置
				XSSFSheet sheet1 = wb.getSheetAt(0);
				// 处理一页中的每一行 第0行就是表中的第一行 一般为名称。
				int k = sheet1.getLastRowNum();
				fis.close();
				fos = new FileOutputStream(file);
				for (int i = 1; i <= list.size(); i++) {
					// 新建一行
					Row row1 = sheet1.createRow(k + i);
					row1.createCell(0).setCellValue(list.get(i - 1).getAdminClass());
					row1.createCell(1).setCellValue(list.get(i - 1).getCode());
					row1.createCell(2).setCellValue(list.get(i - 1).getName());
					row1.createCell(3).setCellValue(list.get(i - 1).getGrade());
				}
				wb.write(fos);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (wb != null) {
						wb.close();
					}
					if (fos != null) {
						fos.close();
					}
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			// 不存在 创建xlsx
			wb = new XSSFWorkbook();
			try {
				fos = new FileOutputStream(file);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			XSSFSheet sheet = wb.createSheet("sheet1");
			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			Cell cell1 = row.createCell(1);
			Cell cell2 = row.createCell(2);
			Cell cell3 = row.createCell(3);
			cell.setCellValue("序号");
			cell1.setCellValue("学号");
			cell2.setCellValue("姓名");
			cell3.setCellValue("离散");
			// excel中的sheet页
			XSSFSheet sheet1 = wb.getSheetAt(0);
			// 获取最后一行的序号
			for (int i = 1; i <= list.size(); i++) {
				// 新建一行
				Row row1 = sheet1.createRow(i);
				row1.createCell(0).setCellValue(list.get(i - 1).getAdminClass());
				row1.createCell(1).setCellValue(list.get(i - 1).getCode());
				row1.createCell(2).setCellValue(list.get(i - 1).getName());
				row1.createCell(3).setCellValue(list.get(i - 1).getGrade());
			}
			try {
				wb.write(fos);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (wb != null) {
						wb.close();
					}
					if (fos != null) {
						fos.close();
					}
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	//导入 由MySQL导入到excel
	@Override
	public void mySQLToExcel(String data_file) {
		MySQLDao mysql = new MySQLDaoImpl();
		mysql.importExcel(data_file);
		
	}

	//excel导出到json文件中
	@Override
	public void outExcel(String data_file,String json_file) {
		List<Achievement> list = new ArrayList();
		list = outExcel(data_file);
		File file = new File(json_file);
		FileWriter fo = null;
		BufferedWriter bw = null;
		try {
			fo = new FileWriter(file);
			bw = new BufferedWriter(fo);
			for (int i = 0; i < list.size(); i++) {
				String string = JSON.toJSONString(list.get(i));
				bw.write(string + "\r\n");
				bw.flush();
			}
			if (logger.isDebugEnabled()) {
				logger.debug("导入完成"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (IOException e) {
			logger.error("exportJson(String)", e); //$NON-NLS-1$
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fo != null)
					fo.close();
			} catch (IOException e) {
				logger.error(e); //$NON-NLS-1$
			}
		}
	}

	//Json文件导入到excel中
	@Override
	public void jsonToExcel(String json_file, String data_file) {
		List<Achievement> list = new ArrayList();
		File file = new File(json_file);
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String string = br.readLine();
			while(string != null) {
				Achievement achievement = JSON.parseObject(string, Achievement.class);
				list.add(achievement);
				string = br.readLine();
			}
			if (logger.isDebugEnabled()) {
				logger.debug("读取Json文件完成"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			logger.error(e); //$NON-NLS-1$
		} catch (IOException e) {
			logger.error(e); //$NON-NLS-1$
		}
		//由列表导入到excel中
		creatExcel(data_file,list);
		if (logger.isDebugEnabled()) {
			logger.debug("Json文件导入excel完成"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
	}

}

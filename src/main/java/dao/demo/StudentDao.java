package dao.demo;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import InputExcel.Achievement;
import basedemo.Student;

public interface StudentDao {
	static final Logger logger = LogManager.getLogger(StudentDao.class.getName());
	//查找名字
	List<Student> findByName(String name);
	//导出到list列表
	List<Achievement> outExcel(String data_file);
	//shuju fangwen jiekou 数据访问对象(Data Access Objects,DAO)DAO
}

package dao.demo;

import java.util.List;

import InputExcel.Achievement;

public interface ExcelToMySQL {
	void MySQL(String data_file);
	void importMySQL(List<Achievement> list);
}

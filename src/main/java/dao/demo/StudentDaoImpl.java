package dao.demo;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import InputExcel.Achievement;
import basedemo.Student;

public class StudentDaoImpl implements StudentDao {

	Connection conn = null;
	ResultSet rs = null;
	Statement st = null;
	PreparedStatement pst = null;
	
	@Override
	public List<Student> findByName(String name) {
		List<Student> ss = new ArrayList<>();
		String URL = "jdbc:mysql://202.196.37.91:3306/course?useUnicode=true&amp;characterEncoding=utf-8";
		String USER = "zutnlp";
		String PASSWORD = "zutnlp";
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			st = conn.createStatement();
			rs = st.executeQuery("SELECT ID,CODE,FULLNAME FROM T_STUDENT");
			// 4.处理数据库的返回结果(使用ResultSet类)
			while (rs.next()) {
				Student student = new Student();
				student.setID(rs.getLong(1));
				student.setName(rs.getString(3));
				student.setCode(rs.getString(2));
				ss.add(student);
//				System.out.println(rs.getLong(1) + " " + rs.getString(2) + " " + rs.getString(3));
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ss;
	}

	@Override
	public List<Achievement> outExcel(String data_file) {
		List<Achievement> studentList = new ArrayList<Achievement>();
		
		/**
		 *   读取Excel
		 */
		InputStream input = ExcelToMySQLImpl.class.getResourceAsStream(data_file);
		// 创建文件对象wb
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(input);
		} catch (IOException e) {
			logger.error(e); //$NON-NLS-1$
		}
		// excel中的sheet页
		XSSFSheet sheet = wb.getSheetAt(0);
		// 处理一页中的每一行 从第二行开始 第一行就是0，第二行就是1
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			// row 一排 处理一排数据
			Row row = sheet.getRow(i);
			Achievement s = new Achievement();
			/**
			 * Set the system unique Id for object
			 */
			// 设置编号
			s.setId(Long.valueOf(1000 + i));
			for (int j = 0; j < row.getLastCellNum(); j++) {
				// cell 小格格 获得小格格数据
				Cell cell = row.getCell(j);
				if (cell != null) {
					cell.setCellType(CellType.STRING);
					String value = row.getCell(j).getStringCellValue().trim();
					if (j == 1)
						s.setCode(value);
					if (j == 2)
						s.setName(value);
					if (j == 0)
						s.setAdminClass(value);
					if (j == 3)
						s.setGrade(value);
				}
			}
			studentList.add(s);
		}
		return studentList;
	}
}

package dao.demo;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import InputExcel.Achievement;

public class ExcelToMySQLImpl implements ExcelToMySQL {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ExcelToMySQLImpl.class.getName());
	Connection conn = null;
	PreparedStatement pst = null;
	
	@Override
	public void MySQL(String data_file) {
		List<Achievement> studentList;
		
//		Connection conn = null;
//		PreparedStatement pst = null;
		
		studentList = new ArrayList<Achievement>();
		
		/**
		 *   读取Excel
		 */
		InputStream input = ExcelToMySQLImpl.class.getResourceAsStream(data_file);
		// 创建文件对象wb
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(input);
		} catch (IOException e) {
			logger.error(e); //$NON-NLS-1$
		}
		// excel中的sheet页
		XSSFSheet sheet = wb.getSheetAt(0);
		// 处理一页中的每一行 从第二行开始 第一行就是0，第二行就是1
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			// row 一排 处理一排数据
			Row row = sheet.getRow(i);
			Achievement s = new Achievement();
			/**
			 * Set the system unique Id for object
			 */
			// 设置编号
			s.setId(Long.valueOf(1000 + i));
			for (int j = 0; j < row.getLastCellNum(); j++) {
				// cell 小格格 获得小格格数据
				Cell cell = row.getCell(j);
				if (cell != null) {
					cell.setCellType(CellType.STRING);
					String value = row.getCell(j).getStringCellValue().trim();
					if (j == 1)
						s.setCode(value);
					if (j == 2)
						s.setName(value);
					if (j == 0)
						s.setAdminClass(value);
					if (j == 3)
						s.setGrade(value);
				}
			}
			studentList.add(s);
		}
		/**
		 *   写入数据库
		 */
		String URL = "jdbc:mysql://127.0.0.1/test?useUnicode=true&amp&useSSL=false";
		String USER = "root";
		String PASSWORD = "lilin123";
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			String s = "insert into t_student(ID,CODE,NAME,LISAN) values(?,?,?,?)";
			for (Achievement achievement : studentList) {
				pst = conn.prepareStatement(s);
				pst.setString(1, achievement.getAdminClass());
				pst.setString(2, achievement.getCode());
				pst.setString(3, achievement.getName());
				pst.setString(4, achievement.getGrade());
				pst.execute();  
			}
		} catch (ClassNotFoundException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("找不到类"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			logger.error(e); //$NON-NLS-1$
		} catch (SQLException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("找不到SQL"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			logger.error(e); //$NON-NLS-1$
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				if (logger.isDebugEnabled()) {
					logger.debug("无法结束"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				logger.error(e); //$NON-NLS-1$
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("导入数据库完成"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	@Override
	public void importMySQL(List<Achievement> list) {
		String URL = "jdbc:mysql://127.0.0.1/test?useUnicode=true&amp&useSSL=false";
		String USER = "root";
		String PASSWORD = "lilin123";
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			String s = "insert into t_student(ID,CODE,NAME,LISAN) values(?,?,?,?)";
			for (Achievement achievement : list) {
				pst = conn.prepareStatement(s);
				pst.setString(1, achievement.getAdminClass());
				pst.setString(2, achievement.getCode());
				pst.setString(3, achievement.getName());
				pst.setString(4, achievement.getGrade());
				pst.execute();  
			}
		} catch (ClassNotFoundException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("找不到类"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			logger.error(e); //$NON-NLS-1$
		} catch (SQLException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("找不到SQL"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			logger.error(e); //$NON-NLS-1$
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				if (logger.isDebugEnabled()) {
					logger.debug("无法结束"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				logger.error(e); //$NON-NLS-1$
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("导入数据库完成"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
	}

}

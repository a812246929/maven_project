package dao.mysqldao;

import java.util.List;

import InputExcel.Achievement;

public interface MySQLDao<T> {
	//由excel导入到数据库
	boolean excelToMySQL(String excel_file); 
	//由csv文件导入到数据库
	boolean csvToMySQL(String csv_file);
	//由json导入到数据库
	boolean jsonToMySQL(String json_file);
	
	//导出到json文件
	boolean mysqlTotJson(String json_file);
	//导出数据库 成csv文件  传入文件地址
	boolean importExcel(String csv_file);
	//导出到list列表 
	List<T> outMySQL();
	
	//增加 数据
	boolean creatMySQL(T t);
	//更新excel 传入跟新数据
	int updateMySQL(T t);
	//删除mysql的数据
	int removeMySQL(String name);
	//查找 按ID查找     (应该按序列化的序号查找)
	T findByID(Long ID);
	//查找 按姓名查找
	List<T> findByName(String name);
}

package dao.mysqldao.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import InputExcel.Achievement;
import dao.exceldao.ExcelDao;
import dao.exceldao.impl.ExcelDaoImpl;
import dao.mysqldao.MySQLDao;

public class MySQLDaoImpl implements MySQLDao<Achievement> {
	protected Logger logger = LogManager.getLogger(this.getClass().getName());

	String url = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
	String user = "root";
	String password = "lilin123";

	Connection conn = null;
	ResultSet rs = null; // 存放返回的结果
	Statement st = null; // 应该是操作的对象
	PreparedStatement pst = null; // 继承类 继承自Statement

	List<Achievement> list = new ArrayList<>();

	public MySQLDaoImpl() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url, user, password);
			st = conn.createStatement();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean importExcel(String csv_file) {
		boolean flag = false;
		List<Achievement> list = outMySQL();
		System.out.println(list);
		// 导出成csv
		try {
			byte[] bs = { (byte) 0xef, (byte) 0xbb, (byte) 0xbf };// UTF-8的 编码 默认无BOM 此行写入文件头变带BOM头
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csv_file), "UTF-8"));
			out.write(new String(bs));// 写入BOM头
			String A, B, C, D;
			for (Achievement i : list) {
				// 判断是否为null
				A = i.getAdminClass();
				B = i.getCode();
				C = i.getName();
				D = i.getGrade();
				if (A == null)
					A = " ";
				if (B == null)
					B = " ";
				if (C == null)
					C = " ";
				if (D == null)
					D = " ";
				out.write(A);
				out.write(",");
				out.write(B);
				out.write(",");
				out.write(C);
				out.write(",");
				out.write(D);
				out.newLine();
			}
			out.flush();
			out.close();
			logger.info("写入成功！");
		} catch (IOException e) {
			e.printStackTrace();
			return flag;
		}
		flag = true;
		return flag;
	}

	@Override
	public int updateMySQL(Achievement achievement) {
		int i = 0;
		String sql = "UPDATE T_STUDENT SET ID='" + achievement.getId() + "' where Name='" + achievement.getName() + "'";
		try {
			pst = (PreparedStatement) conn.prepareStatement(sql);
			i = pst.executeUpdate();
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (logger.isDebugEnabled()) {
			logger.debug("更新数据库完成"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return i;
	}

	@Override
	public int removeMySQL(String name) {
		int i = 0;
		String sql = "DELETE FROM T_STUDENT WHERE NAME ='" + name + "'";
		try {
			pst = conn.prepareStatement(sql);
			i = pst.executeUpdate();
			if (i >= 1) {
				System.out.println("删除成功");
			} else
				System.out.println("删除失败");
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}

	@Override
	public Achievement findByID(Long ID) {
		Achievement achievement = null;
		try {
			String selectSQL = "SELECT ID, CODE, NAME, LISAN FROM T_STUDENT WHERE ID =  " + ID;
			rs = this.st.executeQuery(selectSQL);
			while (rs.next()) {
				achievement = new Achievement();
				achievement.setId(rs.getLong(1));
				achievement.setCode(rs.getString(2));
				achievement.setName(rs.getString(3));
				achievement.setGrade(rs.getString(4));
				if (achievement.getId() == ID) {
					logger.info("已找到ID");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return achievement;
	}

	@Override
	public List<Achievement> findByName(String name) {
		List<Achievement> list = new ArrayList<Achievement>();
		try {
			String selectSQL = "SELECT ID, CODE, NAME, LISAN FROM T_STUDENT WHERE NAME =  '" + name + "'";
			rs = this.st.executeQuery(selectSQL);
			while (rs.next()) {
				Achievement achievement = new Achievement();
				achievement = new Achievement();
				achievement.setId(rs.getLong(1));
				achievement.setCode(rs.getString(2));
				achievement.setName(rs.getString(3));
				achievement.setGrade(rs.getString(4));
				if (StringUtils.equals(achievement.getName(), name)) {
					list.add(achievement);
					if (logger.isDebugEnabled()) {
						logger.debug("已查找到"); //$NON-NLS-1$ //$NON-NLS-2$
					}
				}
			}
		} catch (SQLException e) {
			logger.error("findByName(String)", e); //$NON-NLS-1$
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
			} catch (SQLException e) {
				logger.error("findByName(String)", e); //$NON-NLS-1$
			}
		}
		return list;
	}

	// 由excel导入到MySQL
	@Override
	public boolean excelToMySQL(String File_Data) {
		ExcelDao excel = new ExcelDaoImpl();
		return excel.importMySQL(File_Data);
	}

	// 增加数据
	@Override
	public boolean creatMySQL(Achievement achievement) {
		boolean flag = false;
		try {
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			String s = "insert into t_student(ID,CODE,NAME,LISAN) values(?,?,?,?)";
			pst = conn.prepareStatement(s);
			pst.setString(1, achievement.getAdminClass());
			pst.setString(2, achievement.getCode());
			pst.setString(3, achievement.getName());
			pst.setString(4, achievement.getGrade());
			pst.execute();
		} catch (SQLException e) {
			
			if (logger.isDebugEnabled()) {
				logger.debug("找不到SQL"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			logger.error(e); // $NON-NLS-1$
			return  flag;
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				if (logger.isDebugEnabled()) {
					logger.debug("无法结束"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				logger.error(e); // $NON-NLS-1$
				return  flag;
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("添加数据库完成"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		flag =true;
		return flag;
	}

	@Override
	// 导出到list列表
	public List<Achievement> outMySQL() {
		List<Achievement> list = new ArrayList<>();
		try {
			String selectSQL = "SELECT ID, CODE, NAME, LISAN FROM T_STUDENT ";
			// 生成pst对象
			pst = conn.prepareStatement(selectSQL);
			rs = pst.executeQuery();
			while (rs.next()) {
				Achievement achievement = new Achievement(rs.getString(1), rs.getString(2), rs.getString(3),
						rs.getString(4));
				list.add(achievement);
			}
			// 关闭连接
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public boolean mysqlTotJson(String json_file) {
		boolean flag = false;
		List<Achievement> list = outMySQL();
		File file = new File(json_file);
		FileWriter fo = null;
		BufferedWriter bw = null;
		try {
			fo = new FileWriter(file);
			bw = new BufferedWriter(fo);
			for (int i = 0; i < list.size(); i++) {
				String string = JSON.toJSONString(list.get(i));
				bw.write(string + "\r\n");
				bw.flush();
			}
			System.out.println("导入完成");
		} catch (IOException e) {
			e.printStackTrace();
			return flag;
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fo != null)
					fo.close();
			} catch (IOException e) {
				e.printStackTrace();
				return flag;
			}
		}
		flag = true;
		return flag;
	}

	@Override
	public boolean jsonToMySQL(String File_Data) {
		boolean flag = false;
		List<Achievement> list = new ArrayList();
		File file = new File(File_Data);
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String string = br.readLine();
			while(string != null) {
				Achievement achievement = JSON.parseObject(string, Achievement.class);
				list.add(achievement);
				string = br.readLine();
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return flag;
		} catch (IOException e) {
			e.printStackTrace();
			return flag;
		}
		String s = "INSERT INTO T_STUDENT(ID,CODE,NAME,LISAN) values(?,?,?,?)";
		try {
			for (Achievement achievement : list) {
				pst = conn.prepareStatement(s);
				pst.setString(1, achievement.getAdminClass());
				pst.setString(2, achievement.getCode());
				pst.setString(3, achievement.getName());
				pst.setString(4, achievement.getGrade());
				pst.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return flag;
		} finally {
			try {
				if(pst != null) {
					pst.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				if (logger.isDebugEnabled()) {
					logger.debug("无法结束"); //$NON-NLS-1$ //$NON-NLS-2$
				}
				logger.error(e); // $NON-NLS-1$
				return flag;
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("导入数据库完成"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		flag = true;
		return flag;
	}

	@Override
	public boolean csvToMySQL(String csv_file) {
		boolean flag = false;
		List<Achievement> list = new ArrayList();
		File file = new File(csv_file);
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String string = br.readLine();
			while(string != null) {
				String item[] = string.split(",");
				Achievement achievement = new Achievement(item[0],item[1],item[2],item[3]);
				list.add(achievement);
				string = br.readLine();
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return flag;
		} catch (IOException e) {
			e.printStackTrace();
			return flag;
		}
		for(Achievement i :list) {
			creatMySQL(i);
		}
		flag = true;
		return flag;
	}
}

package basedemo;

import java.io.File;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Student {
	String name;
	String code;
	String sex;
	Long ID;
	public Student() {
		name =null;
		code =null;
		sex =null;
		ID =null;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setID(Long iD) {
		ID = iD;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public Long getID() {
		return ID;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return ID+" "+code+" "+name;
	}
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
}

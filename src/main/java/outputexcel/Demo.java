package outputexcel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import InputExcel.Achievement;
import InputExcel.AchievementMangerImpl;

public class Demo {
	public static void main(String[] args) {
		List<Achievement> list = new ArrayList();
		Achievement achievement = new Achievement("13","201608030333","sdds","25");
		list.add(achievement);
		InputStream input = AchievementMangerImpl.class.getResourceAsStream("/work.xlsx");
		// 创建文件对象wb
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(input);
		} catch (IOException e) {
			e.printStackTrace();  
		}
		// excel中的sheet页
		XSSFSheet sheet = wb.getSheetAt(0);
		// 处理一页中的每一行 从第二行开始 第一行就是0，第二行就是1
		int k = sheet.getLastRowNum(); 
		//获取最后一行的序号
		for (int i = 1; i <= list.size(); i++) {
			// 新建一行
			Row row =sheet.createRow(k+i);
			row.createCell(0).setCellValue(list.get(i-1).getAdminClass());
			row.createCell(1).setCellValue(list.get(i-1).getCode());
			row.createCell(2).setCellValue(list.get(i-1).getName());
			row.createCell(3).setCellValue(list.get(i-1).getGrade());
		}

	}
}

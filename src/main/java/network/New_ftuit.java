package network;

import java.util.Scanner;

public class New_ftuit {
	public static void main(String[] args) {
		
		Fruit f1 = new Fruit(0.86f,"yellow");
		Fruit f2 = new Fruit(0.73f,"red");
		System.out.println(f1.getWeight()+" "+f1.getColor());
		System.out.println(f2.getWeight()+" "+f2.getColor());
		System.out.println((f1.getWeight()+f2.getWeight())/2);
		
		//作业第三题使用构造方法
		Fruit1 f3 = new Fruit1();
		Fruit1 f4 = new Fruit1();
		
		
		//作业第二题普通方法
		float weight3;float weight4;
		String color3;String color4;
		Scanner sc = new Scanner(System.in);
		System.out.println("输入第一个水果的质量和颜色");
		weight3 = sc.nextFloat();
		color3 = sc.next();
		System.out.println("输入第二个水果的质量和颜色");
		weight4 = sc.nextFloat();
		color4 = sc.next();
		f3.setWeight(weight3);
		f3.setColor(color3);
		f4.setWeight(weight4);
		f4.setColor(color4);
		System.out.println(f3.getWeight()+" "+f3.getColor());
		System.out.println(f4.getWeight()+" "+f4.getColor());
		System.out.println((f3.getWeight()+f4.getWeight())/2);
		
		
		//作业第四题继承类
		Apple a1 = new Apple();
		Banana b1 = new Banana();
		Mango m1 = new Mango();
		System.out.println("输入苹果的质量：");
		float weight_a = sc.nextFloat();
		System.out.println("输入香蕉的质量：");
		float weight_b = sc.nextFloat();
		System.out.println("输入芒果的质量：");
		float weight_m = sc.nextFloat();
		a1.setWeight(weight_a);
		b1.setWeight(weight_b);
		m1.setWeight(weight_m);
		System.out.println("苹果 颜色:"+a1.getColor()+" 质量:"+a1.getWeight());
		System.out.println("香蕉 颜色:"+b1.getColor()+" 质量:"+b1.getWeight());
		System.out.println("芒果 颜色:"+m1.getColor()+" 质量:"+m1.getWeight());
	}
}

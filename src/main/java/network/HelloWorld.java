package network;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class HelloWorld {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(HelloWorld.class.getName());

	public static void main(String[] args) {
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - {}", "log4E"); //$NON-NLS-1$ //$NON-NLS-2$

		}
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - {}", "OK"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - {}", "还很OK？"); //$NON-NLS-1$ //$NON-NLS-2$
		}

	}

}

package mooc.filedemo;

import java.io.File;

public class FileDemo {
	public static void main(String[] args) {
		//创建单目录
		File file = new File("F:\\截图");
		file.mkdir();
		//创建多目录
		File file2 = new File("F:\\截图1\\截图2");
		file2.mkdirs();
		File file3 = new File("F:\\截图2");
		System.out.println(file3.exists());
		System.out.println(file3.isDirectory());
		System.out.println(file3.isFile());
	}
}

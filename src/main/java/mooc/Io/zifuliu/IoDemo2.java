package mooc.Io.zifuliu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class IoDemo2 {
	public static void main(String[] args) {
		//字符流
		//read() close()
		File file = new File("test.txt");
		char[] c = null;
		try {
			FileReader fr = new FileReader(file);
			c = new char[512];
			fr.read(c);
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(new String(c));
		
	}
}

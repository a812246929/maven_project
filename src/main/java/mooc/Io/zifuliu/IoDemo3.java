package mooc.Io.zifuliu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class IoDemo3 {
	public static void main(String[] args) {
		//字符流
		//write() append() flush() close()
		File file = new File("test1.txt");
		try {
			FileWriter fw = new FileWriter(file);
			fw.write("nihao");
			fw.append("\r\n你好");
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println();
	}
}

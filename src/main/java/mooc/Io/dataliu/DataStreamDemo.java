package mooc.Io.dataliu;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DataStreamDemo {
	public static void main(String[] args) {
		// 实现多个Person类对象数据的写入和读取操作
		Person[] persons = new Person[] { new Person("Kate", 18), new Person("Tom", 18), new Person("Lucy", 19) };
		File file = new File("person.txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			DataOutputStream dos = new DataOutputStream(fos);
			
			for (int i = 0;i<persons.length;i++) {
				dos.writeUTF(persons[i].name);
				dos.writeChar('\n');
				dos.writeInt(persons[i].age);
			}
			dos.close();
			fos.close();
		} catch (FileNotFoundException e) {
 
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		
		//读取文件
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			DataInputStream dis = new DataInputStream(fis);
			String name = dis.readUTF();
			char n = dis.readChar();
			int a = dis.readInt();
			System.out.println(name+n+a);
		} catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		}
}


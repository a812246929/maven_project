package mooc.Io.zijieliu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.experimental.theories.Theories;

import junit.framework.Test;

public class IoDemo {
	public static void main(String[] args) {
		//FileInputStream
		//read() close()
		//输出为字节。
		File file = new File("test.txt");
		byte[] b = null;
		int f = 0;
		try {
			FileInputStream fos = new FileInputStream(file);
			b = new byte[fos.available()];
			fos.read(b);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(new String(b));
	}
}

package mooc.Io.zijieliu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 * @author Administrator 字节流
 */
public class IoDemo1 {
	public static void main(String[] args) {
		//FileOutputStream
		//write()  close()
		File file = new File("test.txt");
		String str = "写文件测试  \r\nHello Dome";
		try {
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(str.getBytes());
			fos.close();
			System.out.println("写入成功");
		} catch (FileNotFoundException e) {
			System.out.println("文件找不到");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("写数据失败");
			e.printStackTrace();
		}
	}
}

package mooc.Io.collectiondemo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Demo {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		//添加元素
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
		list.add("6");
		list.addAll(list);
		System.out.println(list);
//		System.out.println();
		Iterator iterator = list.iterator();
		while(iterator.hasNext())
		{
			System.out.print(iterator.next()+" ");
		}
		System.out.println();
		
		Set<String> set =new HashSet<String>();
		set.add("1");
		set.add("2");
		set.add("3");
		set.add("4");
		set.add("5");
		set.add("6");
		set.addAll(list);
		System.out.println(set);
		Iterator iterator1 = set.iterator();
		while(iterator1.hasNext())
		{
			System.out.print(iterator1.next()+" ");
		}
		
		
	}
}

package mooc.Io.bufferread;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import junit.framework.Test;

public class BufferedReaderDemo {
	//BufferedReader 传入的参数是Reader
	public static void main(String[] args) {
		File file = new File("Test.txt");
		FileReader  fr = null;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String string = br.readLine();
			while(string != null) {
				System.out.println(string);
				string = br.readLine();
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

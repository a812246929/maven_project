package mooc.collection.setdemo;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	public static void main(String[] args) {
		Set<String> set = new HashSet();
		String s1 = "Hello";
		String s2 = s1;
		String s3 = "World";
		set.add(s1);
		set.add(s2);
		set.add(s3);
		Iterator<String> iterator = set.iterator();
		while(iterator.hasNext())
		{
			System.out.println(iterator.next()+" ");
		}
	}
}

package mooc.collection.treemapdemo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

public class TreeMapDemo {
	public static void main(String[] args) {
		HashMap<String,String> hashmap = new HashMap();
		hashmap.put("THU", "清华大学");
		hashmap.put("CSU", "中南大学");
		hashmap.put("ZZTI", "中原工学院");
		hashmap.put("CUMT", "北京大学");
		
		//遍历
		Set<String> keyset =hashmap.keySet();
		Iterator<String> iterator = keyset.iterator();
		while(iterator.hasNext()) {
			String key = iterator.next();
			System.out.println(key+" "+hashmap.get(key));
		}
		TreeMap<String,String> map = new TreeMap(hashmap);
		System.out.println();
		Set<String> keyset1 =map.keySet();
		Iterator<String> iterator1 = keyset1.iterator();
		while(iterator1.hasNext()) {
			String key = iterator1.next();
			System.out.println(key+" "+hashmap.get(key));
		}
	}
}

package mooc.collection.collectiondemo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Demo {
	public static void main(String[] args) {
		
	List<String> list = new ArrayList<String>();
	list.add("1");
	list.add("2");
	list.add("3");
	list.add("4");
	list.add("5");
	list.add("6");
	list.addAll(list);
	System.out.println(list);
	
	Iterator iterator = list.iterator();
	while(iterator.hasNext()) {
		System.out.print(iterator.next());
	}
	
	System.out.println();
	Set<String> set = new HashSet<String>();
	set.add("1");
	set.add("2");
	set.add("3");
	set.add("4");
	set.add("5");
	set.add("6");
	set.addAll(set);
	System.out.println(set);
	
	//无参toArray
	Object[] str = list.toArray();
	for(int i = 0; i<list.size();i++) {
		System.out.print(str[i]+" ");
	}
	System.out.println();
	
	//有参toArray  参数中的数组小于集合，则参数数组无用，返回一个数组
	String[] str1 = new String[list.size()-1];
	String[] str2 = list.toArray(str1);
	for(int i = 0; i<str1.length;i++) {
		System.out.print(str1[i]+" ");
	}
	System.out.println();
	for(int i = 0; i<str2.length;i++) {
		System.out.print(str2[i]+" ");
	}
	System.out.println();
	}
}

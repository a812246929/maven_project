package mooc.collection.hashmapdemo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class HashMapDemo {
	public static void main(String[] args) {
		HashMap<String,String> hashmap = new HashMap();
		hashmap.put("THU", "清华大学");
		hashmap.put("CSU", "中南大学");
		hashmap.put("ZZTI", "中原工学院");
		hashmap.put("CUMT", "清华大学");
		
		//遍历
		Set<String> keyset =hashmap.keySet();
		Iterator<String> iterator = keyset.iterator();
		while(iterator.hasNext()) {
			String key = iterator.next();
			System.out.println(key+" "+hashmap.get(key));
		}
		
	}
}

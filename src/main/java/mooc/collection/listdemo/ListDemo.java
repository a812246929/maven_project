package mooc.collection.listdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListDemo {
	//list add() addAll() get() IndexOf() lastIndexOf() listIterator()
	//remove() subList() 
	public static void main(String[] args) {
		//List实例化
		List<String> list = new ArrayList<String>();
		//add()
		list.add("1");
		traverse(list);
		//在第一个位置添加一个元素“2”
		list.add(0,"2");
		traverse(list);
		//在第二个位置添加一个元素“8”
		list.add(1, "8");
		traverse(list);
		//在第一个位置添加list
		list.addAll(0, list);
		traverse(list);
		
		//IndexOf() LastIndexOf()
		System.out.println("最先出现2的位置："+ list.indexOf("2"));
		System.out.println("最后出现2的位置："+ list.lastIndexOf("2"));
		//3所在的位置 如果不存在返回-1
		System.out.println("最先出现3的位置： "+list.indexOf("3"));
		
		//listIterator()
		ListIterator<String> listIterator = list.listIterator(2);
		while(listIterator.hasNext()) {
			System.out.print(listIterator.next()+" ");
		}
		System.out.println();
		
		//remove()
		String str = list.remove(1);
		System.out.println("删除的元素："+str);
		traverse(list);
		
		//subList() 一般都是左闭右开 范围不对没有办法取出来 如果相等取出来一个空串
		List<String> list01 = list.subList(2,4);
		traverse(list01);
		
		//替换
		list.set(2, "9");
		traverse(list);
	}
	//遍历
	static void traverse(List<String> list) {
		for(int i =0 ;i < list.size();i++) {
			System.out.print(list.get(i)+" ");
		}
		System.out.println();
	}
} 

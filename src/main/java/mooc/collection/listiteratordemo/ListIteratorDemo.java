package mooc.collection.listiteratordemo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListIteratorDemo {
	static ArrayList<String> init(){
		ArrayList<String> list = new ArrayList<String>();
		list.add("Jack");
		list.add("Tom");
		list.add("Rose");
		list.add("Annie");
		return list;
	}
	static void reverseEnumElement(List<String> list) {
		System.out.println("集合元素如下");
		ListIterator<String> listIterator = list.listIterator(list.size());
		while(listIterator.hasPrevious()) {
			System.out.print(listIterator.previous()+" ");
		}
	}
	public static void main(String[] args) {
		ArrayList<String> list = init();
		reverseEnumElement(list);
		System.out.println(list);
	}
}

package mooc.collection.collectionsdemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CollectionsDemo {
	public static void enumElement(List<String> list) {
		System.out.println("集合元素如下");
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next()+" ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		List<String> list = new ArrayList();
		// addAll向list中添加5个元素
		Collections.addAll(list, "1", "2", "3", "4", "5");
		// 遍历list
		System.out.println("addAll");
		enumElement(list);
		
		
		//reverse()实现集合元素反转
		Collections.reverse(list);
		// 遍历list
		System.out.println("reverse");
		enumElement(list);
		
		//排序
		Collections.sort(list);
		// 遍历list
		System.out.println("sort");
		enumElement(list);
	
		//二分法查找binarySearch()
		System.out.println("\n查找的元素4"+(Collections.binarySearch(list, "4")>=0 ? "存在":"不存在"));
		
		//替换
		Collections.replaceAll(list,"1","A");
		// 遍历list
		System.out.println("replaceAll");
		enumElement(list);
		
	}
}

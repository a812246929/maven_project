package mooc.collection.iteratordemo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IteratorDemo {
	//初始化list
	static ArrayList<String> init(){
		ArrayList<String> list = new ArrayList<String>();
		list.add("Jack");
		list.add("Tom");
		list.add("Rose");
		list.add("Annie");
		return list;
	}
	//遍历
	static void enumElement(List<String> list) {
		System.out.println("集合元素如下");
		Iterator<String> iterator = list.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
	public static void main(String[] args) {
		List<String> list = init();
		enumElement(list);
		Iterator<String> iterator = list.iterator();
		while(iterator.hasNext()) {
			String str = iterator.next();
			if("Tom".equals(str)) {
//				iterator.remove();
				list.remove(str);
				break;
			}
		}
		enumElement(list);
	}
}

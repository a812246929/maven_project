package mooc.collection.arraylistdemo;

import java.util.ArrayList;

public class EnsureCapacityDemo {
	public static void main(String[] args) {
		//先集合中添加100万个元素
		//A
		final int capacity = 1000000;
		ArrayList<String> list = new ArrayList<String>();
		long start = System.currentTimeMillis();
		for(int i = 0; i < capacity;i++) {
			list.add(i+" ");
		}
		long end = System.currentTimeMillis();
		System.out.println("A耗时："+(end-start));
		
		//B
		ArrayList<String> list01 = new ArrayList();
	     long start01 = System.currentTimeMillis();
	      list01.ensureCapacity(capacity);// 预先设置list大小(实际容量)
	      for (int i = 0; i < capacity; i++) {
	            list01.add(i+" ");
	       }
	     long end01 = System.currentTimeMillis();
	     System.out.println("B耗时："+(end01-start01));
		//C
		ArrayList<String> list02 = new ArrayList<String>();
		long start02 = System.currentTimeMillis();
		for(int i = 0; i < capacity;i++) {
			list02.add(i+" ");
		}
		list01.ensureCapacity(capacity);
		long end02 = System.currentTimeMillis();
		System.out.println("C耗时："+(end02-start02));
		

//		final int capacity = 5000000;
        Object obj = new Object();
       // 没用调用ensureCapacity()方法初始化ArrayList对象
       ArrayList<String> list1 = new ArrayList();
      long startTime = System.currentTimeMillis();
      for (int i = 0; i < capacity; i++) {
             list1.add(i+" ");
      }
      long endTime = System.currentTimeMillis();
      System.out.println("没有调用ensureCapacity()方法所用时间：" + (endTime - startTime) + "ms");
       // 调用ensureCapacity()方法初始化ArrayList对象
      ArrayList<String> list2 = new ArrayList();
      startTime = System.currentTimeMillis();
      list2.ensureCapacity(capacity);// 预先设置list大小(实际容量)
      for (int i = 0; i < capacity; i++) {
            list2.add(i+" ");
       }
      endTime = System.currentTimeMillis();
     System.out.println("调用ensureCapacity()方法所用时间：" + (endTime - startTime) + "ms");
	}
}

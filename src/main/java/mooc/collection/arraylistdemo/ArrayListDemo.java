package mooc.collection.arraylistdemo;

import java.util.ArrayList;

public class ArrayListDemo {
	public static void main(String[] args) {
		//ArrayList 动态数组
		//clone()浅复制
		ArrayList<StringBuffer> list = new ArrayList<StringBuffer>();
		StringBuffer sb = new StringBuffer("Learnling-");
		list.add(0,sb);
		traverse(list);
		ArrayList<StringBuffer> list01 = (ArrayList<StringBuffer>)list.clone();
		traverse(list01);
		
		//修改list中的第一个元素的值
		sb = list.get(0);
		sb.append("ArrayList clone() method");
		//遍历
		traverse(list);
		traverse(list01);
		
		//如果add()方法list中添加 "second value"
		list.add( new StringBuffer("second value"));
		//遍历
		traverse(list);
		traverse(list01);
	}
	static void traverse(ArrayList<StringBuffer> list) {
		System.out.println("~~~集合元素~~~~");
		for(int i = 0;i<list.size();i++) {
			System.out.print(list.get(i)+" ");
		}
		System.out.println();
	}
}

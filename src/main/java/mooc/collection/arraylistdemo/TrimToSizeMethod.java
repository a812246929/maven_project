package mooc.collection.arraylistdemo;

import java.util.ArrayList;

public class TrimToSizeMethod {
	public static void main(String[] args) {
		//trimToSize()方法使用
		//该方法会精简空间
		ArrayList<Integer> list = new ArrayList<Integer>();
		//list中添加3个元素
		list.add(1);
		list.add(2);
		list.add(3);
		list.trimToSize();
		for(int i = 0;i<list.size();i++) {
			System.out.print(list.get(i)+" ");
		}
		System.out.println();
		
	}
}

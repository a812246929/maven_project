package mooc.collection.treeset;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

//自定义比较器
class MyComparator implements Comparator<String> {

	@Override
	public int compare(String str1, String str2) {
		// TODO 自动生成的方法存根
		return str1.length() - str2.length();
	}

}
//按字符串长短比较 需要自定义比较器
public class TreeSetDemo2 {

	public static void main(String[] args) {
		TreeSet<String> treeset = new TreeSet(new MyComparator());
		treeset.add("Jack");
		treeset.add("abc");
		treeset.add("hellow");
		
		traverse(treeset);
	}
	static void traverse(TreeSet<String> treeset) {
		System.out.println("集合中元素如下");
		Iterator<String> iterator = treeset.iterator();
		while(iterator.hasNext()) {
			System.out.print(iterator.next()+" ");
		}
	}
}

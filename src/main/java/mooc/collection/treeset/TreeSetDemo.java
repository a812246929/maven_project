package mooc.collection.treeset;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

import mooc.base.Student;

public class TreeSetDemo {
	public static void main(String[] args) {
		TreeSet<Integer> treeset = new TreeSet();
		treeset.add(20);
		treeset.add(18);
		treeset.add(23);
		treeset.add(22);
		treeset.add(17);
		treeset.add(24);
		treeset.add(19);
		treeset.add(18);
		treeset.add(24);
		
		traverse(treeset);
	}
	static void traverse(TreeSet<Integer> treeset) {
		System.out.println("集合中元素如下");
		Iterator<Integer> iterator = treeset.iterator();
		while(iterator.hasNext()) {
			System.out.print(iterator.next()+" ");
		}
	}
}

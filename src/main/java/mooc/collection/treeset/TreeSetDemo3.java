package mooc.collection.treeset;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

import mooc.base.Student;

public class TreeSetDemo3 {
	
	//放入treeset中的类必须要能比较 
	public static void main(String[] args) {
		TreeSet<Student> treeset = new TreeSet();
		Student stu1 = new Student("201608030301", "123");
		Student stu2 = new Student("201608030302", "456");
		Student stu3 = new Student("201608030303", "123");

		treeset.add(stu1);
		treeset.add(stu2);
		treeset.add(stu3);

		traverse(treeset);
	}
	static void traverse(TreeSet<Student> treeset) {
		System.out.println("集合中元素如下");
		Iterator<Student> iterator = treeset.iterator();
		while (iterator.hasNext()) {
			Student stu = iterator.next();
			System.out.println(stu);
		}
	}
}

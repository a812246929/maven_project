package mooc.collection.hashsetdemo;

import java.util.HashSet;
import java.util.Iterator;

import mooc.base.Student;

public class HashSetDemo {
	public static void main(String[] args) {
		HashSet<Student> hashset = new HashSet();
		Student stu1 = new Student("201608030301", "123");
		Student stu2 = new Student("201608030302", "456");
		Student stu3 = new Student("201608030303", "123");

		hashset.add(stu1);
		hashset.add(stu2);
		hashset.add(stu3);

		traverse(hashset);

	}

	static void traverse(HashSet<Student> hashset) {
		System.out.println("集合中元素如下");
		Iterator<Student> iterator = hashset.iterator();
		while (iterator.hasNext()) {
			Student stu = iterator.next();
			System.out.println(stu);
		}
	}
}

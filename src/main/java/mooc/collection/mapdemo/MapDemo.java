package mooc.collection.mapdemo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapDemo {
	static class Student{
		String name;
		String code;
		Student(String code,String name){
			this.name = name;
			this.code = code;
		}
	}
	public static void main(String[] args) { 
		Student[] stu = new Student[10] ;
		for(int i =0;i<stu.length;i++) {
			stu[i] = new Student("100"+i,"li"+i);
		}
		Map<String,Student> map = new HashMap<String,Student>();
		for(int i = 0;i<stu.length;i++) {
			map.put(stu[i].code, stu[i]);
		}
		//通过set遍历
		Set<Entry<String,Student>> set = map.entrySet();
		Iterator<Entry<String,Student>> iterator = set.iterator();
		while(iterator.hasNext())
		{
			Entry<String,Student> entry = iterator.next();
			System.out.println(entry.getKey());
		}
		
		//toArray()方法输出
		System.out.println("~~~~~");
		Object[] obj = set.toArray();
		for(int i= 0;i<obj.length;i++) {
			Entry<String,Student> entry = (Entry<String,Student>)obj[i];
			System.out.println(entry.getKey());
		}
		
	}
	
}

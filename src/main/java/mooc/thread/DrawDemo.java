package mooc.thread;

public class DrawDemo {
	public static void main(String[] args) throws InterruptedException {
		Account account = new Account();
		
		Thread thread1 = new Thread(new DrawMoney(account,800),"柜台取钱线程");
		Thread thread2 = new Thread(new DrawMoney(account,800),"ATM取钱线程");
		Thread thread3 = new Thread(new SaveMoney(account,1000),"柜台存钱线程");
		Thread thread4 = new Thread(new SaveMoney(account,1000),"ATM存钱线程");

		thread1.start();
//		thread2.sleep(100);
		thread2.start();
		thread3.start();
		thread4.start();
	}
}

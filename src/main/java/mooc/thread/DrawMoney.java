package mooc.thread;

public class DrawMoney implements Runnable {
	private Account account;
	private double amount;
	
	public DrawMoney() {
		// TODO 自动生成的构造函数存根
	}
	
	public DrawMoney(Account account, double amount) {
		super();
		this.account = account;
		this.amount = amount;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public void run() {
		this.account.drawMoney(amount);
	}

}

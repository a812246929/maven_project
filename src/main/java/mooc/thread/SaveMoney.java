package mooc.thread;

public class SaveMoney implements Runnable {
	private Account account;
	private double amount;
	
	public SaveMoney() {
		// TODO 自动生成的构造函数存根
	}
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public SaveMoney(Account account, double amount) {
		super();
		this.account = account;
		this.amount = amount;
	}

	@Override
	public void run() {
		this.account.saveMoney(amount);
	}

}

package mooc.thread;

public class Account {
	//synchronized锁代码块   锁方法
	
	private double balance = 1000;

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void drawMoney(double amount) {
		synchronized (this) {
			System.out.println(Thread.currentThread().getName() + "执行取钱操作：");
			double newBalance = this.balance - amount;

			if (newBalance >= 0) {
				this.balance = newBalance;
				System.out.println("取钱" + amount + "成功，账户余额为：" + this.balance);
			} else {
				System.out.println("取钱" + amount + "失败，账户余额不足");
			}
		}
	}

	public synchronized void saveMoney(double amount) {
		System.out.println(Thread.currentThread().getName() + "执行存钱操作");
		double oldBalance = this.balance;
		double newBalance = oldBalance + amount;
		this.balance = newBalance;
		System.out.println("存钱" + amount + "成功，账户余额为：" + this.balance);

	}
}

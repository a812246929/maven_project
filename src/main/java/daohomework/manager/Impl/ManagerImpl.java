package daohomework.manager.Impl;

import java.util.List;


import daohomework.dao.Dao;
import daohomework.dao.impl.DaoImpl;
import daohomework.eneity.Achievement;
import daohomework.manager.Manager;

public class ManagerImpl implements Manager {
	
	Dao dao = new DaoImpl();
	
	@Override
	public boolean add(Achievement achievement) {
		return dao.add(achievement);
	}

	@Override
	public boolean remove(String name) {
		if(dao.remove(name)>=1) 
			return true;
		else 
			return false;
	}

	@Override
	public boolean update(Achievement achievement) {
		if(dao.update(achievement)>=1) 
			return true;
		else 
			return false;
	}

	@Override
	public Achievement findByID(Long ID) {
		return  (Achievement) dao.findByID(ID);
	}

	@Override
	public List<Achievement> findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public boolean importexcel(String excel_file) {
		return dao.importexcel(excel_file);
	}

	@Override
	public boolean importcsv(String csv_file) {
		return dao.importcsv(csv_file);
	}

	@Override
	public boolean importjson(String json_file) {
		return dao.importjson(json_file);
	}

	@Override
	public boolean exportJson(String json_file) {
		return dao.exportJson(json_file);
	}

	@Override
	public boolean exportExcel(String csv_file) {
		return dao.exportExcel(csv_file);
	}

	@Override
	public List<Achievement> exportList() {
		return dao.exportList();
	}

}

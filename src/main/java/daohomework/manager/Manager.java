package daohomework.manager;

import java.util.List;

import daohomework.eneity.Achievement;



public interface Manager {
	// 增
	boolean add(Achievement achievement);
	// 删
	boolean remove(String name);
	// 改
	boolean update(Achievement achievement);
	// 查
	Achievement findByID(Long ID);
	List<Achievement> findByName(String name);

	// 导入
	boolean importexcel(String excel_file);
	boolean importcsv(String csv_file);
	boolean importjson(String json_file);

	// 导出
	boolean exportJson(String json_file);
	boolean exportExcel(String csv_file);
	List<Achievement> exportList();
}

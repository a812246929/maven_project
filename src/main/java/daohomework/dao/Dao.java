package daohomework.dao;

import java.util.List;

public interface Dao<Achievement> {
	//由excel导入到数据库
		boolean importexcel(String excel_file); 
		//由csv文件导入到数据库
		boolean importcsv(String csv_file);
		//由json导入到数据库
		boolean importjson(String json_file);
		
		//导出到json文件
		boolean exportJson(String json_file);
		//导出数据库 成csv文件  传入文件地址
		boolean exportExcel(String csv_file);
		//导出到list列表 
		List<Achievement> exportList();
		
		//增加 数据
		boolean add(Achievement t);
		//更新excel 传入跟新数据
		int update(Achievement t);
		//删除mysql的数据
		int remove(String name);
		//查找 按ID查找     (应该按序列化的序号查找)
		Achievement findByID(Long ID);
		//查找 按姓名查找
		List<Achievement> findByName(String name);
}

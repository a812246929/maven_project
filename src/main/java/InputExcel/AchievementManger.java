package InputExcel;

import java.util.List;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

public interface AchievementManger //extends BaseManager<Long , Achievement> 
{
	public List getlist();
	void load(String data_file);
	public Achievement findByFullname(String name);
	public Achievement findByPostcode(String code);
}

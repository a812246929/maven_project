package InputExcel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class AchievementMangerImpl implements AchievementManger {
	protected Logger logger = LogManager.getLogger(this.getClass().getName());

	List<Achievement> studentList;
	Map<String ,Achievement> nameMap;
	Map<String ,Achievement> codeMap;
	public AchievementMangerImpl(){
//		this.studentList = new ArrayList<Achievement>();
//		this.map = new HashedMap<>();
	}
	
	public List getlist() {
		return studentList;
	}
	
	public void load(String data_file) {
		this.studentList = new ArrayList<Achievement>();
		this.nameMap = new HashedMap<>();
		this.codeMap = new HashedMap<>();
		InputStream input = AchievementMangerImpl.class.getResourceAsStream(data_file);
		//创建文件对象wb
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//excel中的sheet页
		XSSFSheet sheet = wb.getSheetAt(0);
		//处理一页中的每一行 从第二行开始 第一行就是0，第二行就是1
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			//row 一排 处理一排数据
			Row row = sheet.getRow(i);
			Achievement s = new Achievement();
			/**
			 * Set the system unique Id for object
			 */
			//设置编号
			s.setId(Long.valueOf(1000 + i));
			for (int j = 0; j < row.getLastCellNum(); j++) {
				//cell 小格格 获得小格格数据
				Cell cell = row.getCell(j);
				if (cell != null) {
					cell.setCellType(CellType.STRING);
					String value = row.getCell(j).getStringCellValue().trim();
					if (j == 1)
						s.setCode(value);
					if (j == 2)
						s.setName(value);
					if (j == 0)
						s.setAdminClass(value);
					if (j == 3)
						s.setGrade(value);
				}
			}
			logger.error(" student_" + i + " is :" + s);
			this.studentList.add(s);
			this.nameMap.put(s.getName(), s);
			this.codeMap.put(s.getCode(), s);
		}
		logger.error("size of student list is :" + this.studentList.size());

	}
	public Achievement findByFullname(String Name) {
		for(String name : nameMap.keySet()) {
			if(name.equals(Name))
				return nameMap.get(name);
		}
		return null;
	}
	public Achievement findByPostcode(String code) {
		for(String Code : codeMap.keySet()) {
			if(code.equals(Code))
				return codeMap.get(code);
		}
		return null;
	}

}

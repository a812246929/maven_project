package InputExcel;

import com.alibaba.fastjson.annotation.JSONField;

import base.BaseExcel;

/**
 * 
 * @author liruilin
 *
 */
public class Achievement extends BaseExcel<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2799307951280434053L;

	/**
	 * administrative class
	 */
	@JSONField(name ="adminClass", ordinal = 1)
	String adminClass;
	
	/**
	 * student Id
	 */
	@JSONField(name ="code", ordinal = 2)
	String code;

	/**
	 * student name;
	 */
	@JSONField(name ="name", ordinal = 3)
	String name;
	
	/**
	 * student grade;
	 */
	@JSONField(name ="grade", ordinal = 4)
	String grade;
	public Achievement(){
	}
	public Achievement(String adminClass,String code,String name,String grade){
		this.adminClass = adminClass;
		this.code = code;
		this.name = name;
		this.grade = grade;
	}
	
	public String getAdminClass() {
		return adminClass;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public String getGrade() {
		return grade;
	}

	public void setAdminClass(String adminClass) {
		this.adminClass = adminClass;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setGrade(String grade) {
		this.grade = grade;
	}

}

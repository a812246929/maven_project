package sockmessage.activemq;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

public class ProducerQueen {
	// 结果集
	static ResultSet rs;
	// sql语句
	static String sql = "SELECT NAME FROM T_TXT WHERE ID =  '" + 1004 + "'";

	StringBuffer sb = new StringBuffer();

	public void testMQProducerQueue() throws Exception {
		// 1、创建工厂连接对象，需要制定ip和端口号
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		// 2、使用连接工厂创建一个连接对象
		Connection connection = connectionFactory.createConnection();
		// 3、开启连接
		connection.start();
		// 4、使用连接对象创建会话（session）对象
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		// 5、使用会话对象创建目标对象，包含queue和topic（一对一和一对多）
		Queue queue = session.createQueue("test-queue");
		// 6、使用会话对象创建生产者对象
		MessageProducer producer = session.createProducer(queue);
		// 7、使用会话对象创建一个消息对象
		// 调用数据库
		String string = exporttxt(sql);
//        System.out.println(string);
		TextMessage textMessage = session.createTextMessage(string);
		// 8、发送消息
		producer.send(textMessage);
		// 9、关闭资源
		producer.close();
		session.close();
		connection.close();
	}

	public static String exporttxt(String sql) {
		java.sql.Connection conn = null;
		PreparedStatement pst = null; // 继承类 继承自Statement
		String URL = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
		String USER = "root";
		String PASSWORD = "lilin123";
		StringBuffer sb = new StringBuffer();
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）

			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				sb.append(rs.getString("NAME"));
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
package sockmessage.network;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import sockmessage.activemq.ConsumerQueen;

public class Client {
	public static void client() throws UnknownHostException, IOException {
		Socket client = new Socket("192.168.43.180", 8181);// 服务器在本地，端口8888
		// 接收
		DataInputStream fromserver = new DataInputStream(client.getInputStream());
		// 发送
		DataOutputStream toserver = new DataOutputStream(client.getOutputStream());
		//启动消费者
       /* System.out.println(fromserver.readUTF());
        toserver.writeUTF("我的queenname是多少");*/
        String queenname= fromserver.readUTF();
		ConsumerQueen cq = new ConsumerQueen();
		toserver.writeUTF("成功接收数据");
		try {
			cq.TestMQConsumerQueue(queenname,queenname);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

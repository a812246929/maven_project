package sockmessage.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

public class Server {

	public static void server () throws Exception {
		 ServerSocket server=new ServerSocket(8181);
		while(true) {
			System.out.println("服务器等待连接时间为"+new Date());
			Socket client=server.accept(); //等待客户端
		    Thread threads=new Thread(new MysqlData(client));
		    threads.start();
		}
	}
}

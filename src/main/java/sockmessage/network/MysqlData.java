package sockmessage.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sockmessage.activemq.ProducerQueen;

public class MysqlData implements Runnable {
	Connection conn = null;
	ResultSet rs = null;
	private Socket client;

	public MysqlData(Socket client) {
		// TODO Auto-generated constructor stub
		this.client = client;
	}

	@Override
	public void run() {
		try {
			DataInputStream fromclient = new DataInputStream(client.getInputStream());
			DataOutputStream toclient = new DataOutputStream(client.getOutputStream());
			
			toclient.writeUTF("连接成功！");
			System.out.println(fromclient.readUTF());
			toclient.writeUTF("");
			/*System.out.println("客户端说：" + fromclient.readUTF());
			String sql="SELECT NAME FROM T_TXT WHERE  ID =  '" +1004 + "'"; 
			toclient.writeUTF(exporttxt(sql)); System.out.println("成功发送到客户端！");*/
			 
			//启动生产者
			ProducerQueen pq = new ProducerQueen();
			pq.testMQProducerQueue();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	public String exporttxt(String sql) {
		java.sql.Connection conn = null;
		PreparedStatement pst = null; // 继承类 继承自Statement
		String URL = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
		String USER = "root";
		String PASSWORD = "lilin123";
		StringBuffer sb = new StringBuffer();
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）

			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				sb.append(rs.getString("NAME"));

			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
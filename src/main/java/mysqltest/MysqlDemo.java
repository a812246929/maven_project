package mysqltest;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import basedemo.Student;

import java.sql.Connection;

public class MysqlDemo {	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {	
		Connection conn = null;
		ResultSet rs = null;
		Statement st = null;
		
		String URL = "jdbc:mysql://202.196.37.91:3306/course?useUnicode=true&amp;characterEncoding=utf-8";
		String USER = "zutnlp";
		String PASSWORD = "zutnlp";
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			st = conn.createStatement();
			rs = st.executeQuery("SELECT ID,CODE,FULLNAME FROM T_STUDENT");
			// 4.处理数据库的返回结果(使用ResultSet类)
			System.out.println("ID      CODE      FULLNAME");
			while (rs.next()) {
				System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
			}
			// 关闭资源
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				st.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

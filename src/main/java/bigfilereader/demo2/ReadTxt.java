package bigfilereader.demo2;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel.MapMode;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ReadTxt implements Runnable {
	// 定义字节数组（取水的竹筒）的长度
		private final int BUFF_LEN = 300;
		// 定义读取的起始点
		long start;
		// 定义读取的结束点
		long end;
		// 将读取到的字节输出到raf中 randomAccessFile可以理解为文件流，即文件中提取指定的一部分的包装对象
		RandomAccessFile raf;
		//存放读取的内容 按线程名字存放
		static Map<String, StringBuffer> map = new HashMap();
		
		CyclicBarrier cb ;
		
		public ReadTxt(StartEndPair pair, String name, String r,CyclicBarrier cb) throws FileNotFoundException {
			this.start = pair.start;
			this.end = pair.end;
			this.raf = new RandomAccessFile(name, r);
			this.cb = cb;
		}

		@Override
		public void run() {
			byte[] bt = new byte[BUFF_LEN];
			//存放每次循环的开始位置
			long starttemp = start;
			// 本线程读取的长度
			long len = end - start;
			// i 是循环的次数 需要读取几个BUFF_LEN
			long i = len / BUFF_LEN;
			//创建一个StringBuffer存放本线程读取的东西
			StringBuffer sb = new StringBuffer();
			try {
//				raf.seek(start);
				
				MappedByteBuffer mapBuffer = raf.getChannel().map(MapMode.READ_ONLY,start, len);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				
				
//				for (int j = 0; j < i; j++) {
//					//读取的是字节 l
//					raf.read(bt);
//					byte tmp = raf.readByte();
//					if(tmp=='\n' || tmp=='\r'){
//
//					}
//					//转换成字符串
//					String string = new String(bt, "UTF-8");
//					//添加到sb中
//					sb.append(string);
//					starttemp = starttemp + BUFF_LEN;
//				}
//				raf.seek(start+(i*BUFF_LEN));
//				String string1 = new String(bt, "UTF-8");
//				byte[] temp = new byte[(int) (len % BUFF_LEN)];
//				raf.readFully(temp, 0, (int) (len % BUFF_LEN));
//				String string = new String(temp, "UTF-8");
//				sb.append(string);
				map.put(Thread.currentThread().getName(), sb);
				System.out.println(Thread.currentThread().getName()+"读取文件完成");
				cb.await();//测试性能用
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
			try {
				raf.close();
			} catch (IOException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}

}

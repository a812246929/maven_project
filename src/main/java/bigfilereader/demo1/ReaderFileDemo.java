package bigfilereader.demo1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicLong;


/**
 *  分片了 但是线程内没有分片 依旧有部分乱码
 *  使用的是randomaccessfile 进行操作的
 *  该方法使用 分片一个类  存放开始结束位置一个类  runnable一个类 可能实现了线程的互等
 * */
public class ReaderFileDemo {
	public static void main(String[] args) throws IOException {
		CyclicBarrier cyclicBarrier;
		AtomicLong counter = new AtomicLong(0);
		
		int threadsize = 5;
		String filename = "E:/159.txt";
		Map<Integer,StartEndPair> slicemap = new HashMap();
		Slice slice = new Slice(filename,threadsize);
		slice.SliceTxt(3, slice.threadlen);
		slicemap = slice.fanhuiMap();
		final long startTime = System.currentTimeMillis();
		cyclicBarrier = new CyclicBarrier(threadsize,new Runnable() {
			
			@Override
			public void run() {
				System.out.println("use time: "+(System.currentTimeMillis()-startTime));
				System.out.println("all line: "+counter.get());
				StringBuffer sb = new StringBuffer();
				for(int j = 0;j<threadsize;j++) {
					sb.append(ReadTxt.map.get("a"+j));
//					System.out.println("a"+j+" "+ReadTxt.map.get("a"+j));
				}
				importtxt(sb);
//				System.out.println(sb.toString());
			}
		});
		
		for(int j =0;j<threadsize;j++) {
			StartEndPair pair =slicemap.get(j);
			if(j == threadsize-1) {
				Thread thread = new Thread(new ReadTxt(pair, filename,"r",cyclicBarrier),"a"+j);
				thread.start();
				break;
			}
			Thread thread = new Thread(new ReadTxt(pair, filename,"r",cyclicBarrier),"a"+j);
			thread.start();
		}	

 	}
	public static void importtxt(StringBuffer sb) {
		Connection conn = null;
		PreparedStatement pst = null; // 继承类 继承自Statement
		String URL = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
		String USER = "root";
		String PASSWORD = "lilin123";
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			String s = "insert into t_txt(NAME) values(?)";
			pst = conn.prepareStatement(s);
			String s1 = sb.toString();
			pst.setString(1, s1);
			pst.execute();
			System.out.println("导入完成");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

package bigfilereader.test.txtdao.txtmanager.impl;

import bigfilereader.test.txtdao.dao.TxtDao;
import bigfilereader.test.txtdao.dao.impl.TxtDaoImpl;
import bigfilereader.test.txtdao.txtmanager.TxtManager;

public class TxtManagerImpl implements TxtManager {
	TxtDao dao = new TxtDaoImpl();

	@Override
	public boolean importTxt(StringBuffer txt,int fileID) {
		if (dao.importTxt(txt,fileID))
			return true;
		else
			return false;
	}

}

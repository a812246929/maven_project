package bigfilereader.test.txtdao.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bigfilereader.test.txtdao.dao.TxtDao;

public class TxtDaoImpl implements TxtDao {
	String URL = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
	String USER = "root";
	String PASSWORD = "lilin123";

	Connection conn = null;
	PreparedStatement pst = null; // 继承类 继承自Statement

	public TxtDaoImpl() {
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean importTxt(StringBuffer txt,int fileID) {
		try {
			String sql = "insert into t_txt(NAME,ID) values(?,?)";
			pst = conn.prepareStatement(sql);
			String s1 = txt.toString();
			pst.setString(1, s1);
			pst.setInt(2, fileID);
			pst.execute();
			System.out.println("导入完成");

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}

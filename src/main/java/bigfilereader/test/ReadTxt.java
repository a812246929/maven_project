package bigfilereader.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicLong;

import bigfilereader.test.impl.ReadBigFileImpl;

public class ReadTxt implements Runnable {
	// 定义字节数组（取水的竹筒）的长度
	private final int BUFF_LEN = 1024 * 1024;
	//定义一个类 存放开始 结束的位置
	StartEndPair pair;
	// 将读取到的字节输出到raf中 randomAccessFile可以理解为文件流，即文件中提取指定的一部分的包装对象
	RandomAccessFile raf;
	// 临时字节数组
	byte[] bttemp = new byte[2 * BUFF_LEN];
	// 存放读取的内容 按线程名字存放
	public static Map<String, StringBuffer> map = new HashMap();

	CyclicBarrier cb;
	private  AtomicLong counter = new AtomicLong(0);

	public ReadTxt(StartEndPair pair, String name, String r, CyclicBarrier cb,AtomicLong counter) throws FileNotFoundException {
		this.pair = pair;
		this.raf = new RandomAccessFile(name, r);
		this.cb = cb;
		this.counter = new AtomicLong();
		this.counter = counter;
	}

	@Override
	public void run() {
		byte[] bt = new byte[BUFF_LEN];
		// 每次读取的长度
		long len = pair.end - pair.start;
		// i 是循环的次数 需要读取几个BUFF_LEN
		long i = len / BUFF_LEN;
		// 创建一个StringBuffer存放本线程读取的东西
		StringBuffer sb = new StringBuffer();
		try {
			raf.seek(pair.start);
			//用于断句中临时存放字符数组的开始位置，每一次取一定长度后就开始循环判断字节是否是换行
			int counttemp = 0;
//			System.out.println(Thread.currentThread().getName() + " 线程内部分多少片 " + i);
			for (int j = 0; j < i; j++) {
				// 读取的是字节 l
				raf.read(bt);
				for (int z = 0; z < BUFF_LEN; z++) {
					byte tmp = bt[z];
					//判断字节是否是换行回车 换成 if (tmp == '\n') 也可以运行
					if (tmp == '\n'|| tmp == '\r') {
						bttemp[counttemp++] = bt[z];
						String string = new String(bttemp, 0, counttemp, "UTF-8");
						sb.append(string);
						counttemp = 0;
						counter.incrementAndGet();
					} else {
						bttemp[counttemp++] = bt[z];
					}
				}
			}
			raf.seek(pair.start + (i * BUFF_LEN));
			// 临时的bttemp 字节数组加上 最后一次所需读的长度的数组
			byte[] temp = new byte[(int) (len % BUFF_LEN)];
			raf.readFully(bttemp, counttemp, (int) (len % BUFF_LEN));
			String string = new String(bttemp, 0, counttemp + (int) (len % BUFF_LEN), "UTF-8");
			sb.append(string);
			// 存放线程对应的字符串
			map.put(Thread.currentThread().getName(), sb);
			System.out.println(Thread.currentThread().getName() + "读取文件完成");
//			System.out.println(sb.toString());
			cb.await();// 测试性能用
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		} finally {
			try {
				raf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}

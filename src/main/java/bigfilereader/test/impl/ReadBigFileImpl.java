package bigfilereader.test.impl;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicLong;

import bigfilereader.test.Slice;
import bigfilereader.test.StartEndPair;
import bigfilereader.test.txtdao.txtmanager.TxtManager;
import bigfilereader.test.txtdao.txtmanager.impl.TxtManagerImpl;
import bigfilereader.test.ReadBigFile;
import bigfilereader.test.ReadTxt;

public class ReadBigFileImpl implements ReadBigFile {
	CyclicBarrier cyclicBarrier;
	AtomicLong counter = new AtomicLong(0);
	Map<Integer, StartEndPair> slicemap = new HashMap();
	int i =0;

	@Override
	public boolean readBigFile(String filename, int threadsize,int fileID) throws Exception {
		Slice slice = new Slice(filename, threadsize);
		slicemap =slice.sliceTxT();
		final long startTime = System.currentTimeMillis();
		cyclicBarrier = new CyclicBarrier(threadsize, new Runnable() {

			@Override
			public void run() {
				System.out.println("use time: " + (System.currentTimeMillis() - startTime));
				System.out.println("all line: " +counter.get() );
				i =1;
				
			}
		});

		for (int j = 0; j < threadsize; j++) {
			StartEndPair pair = slicemap.get(j);
			if (j == threadsize - 1) {
				Thread thread = new Thread(new ReadTxt(pair, filename, "r", cyclicBarrier,counter), "a" + j);
				thread.start();
				break;
			}
			Thread thread = new Thread(new ReadTxt(pair, filename, "r", cyclicBarrier,counter), "a" + j);
			thread.start();
		}
		StringBuffer sb = new StringBuffer();
		while(i!=1) {
			
		}
		for (int j = 0; j < threadsize; j++) {
			sb.append(ReadTxt.map.get("a" + j));
//					System.out.println("a"+j+" "+ReadTxt.map.get("a"+j));
		}
				System.out.println(sb.toString());
		TxtManager manger = new TxtManagerImpl();
		if (manger.importTxt(sb,fileID))
			return true;
		else
			return false;
		
	}

}

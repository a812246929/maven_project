package bigfilereader.test;

import java.io.FileNotFoundException;

public interface ReadBigFile {
	public boolean readBigFile(String filename,int threadsize,int fileID) throws FileNotFoundException, Exception; 
}

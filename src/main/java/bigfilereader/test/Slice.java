package bigfilereader.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

public class Slice {
	// 随机读取文件
	RandomAccessFile raf;
	//线程数量
	int threadsize;
	// 开始的位置
	long start;
	// 文件长度
	long filelen;
	//每个线程需要读的长度
	long threadlen;
	// 文件名字
	String filename;
	//存放分段
	Map<Integer,StartEndPair> map = new HashMap();
	//
	int count =0;

	public Slice() {
		// TODO 自动生成的构造函数存根
	}

	public Slice(String filename,int threadsize) {
		this.filename = filename;
		try {
			this.raf = new RandomAccessFile(filename, "r");
			this.filelen = raf.length();
			this.threadsize = threadsize;
			this.threadlen = (filelen-3)/threadsize;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Map sliceTxT() throws Exception {
		sliceTxt(3,threadlen);
		return map;
	}
	
	public void sliceTxt(long a,long b) throws IOException {
		//判断是否到结尾了
				if(a>filelen){
					raf.close();
					return;
				}
				StartEndPair pair = new StartEndPair();
				pair.start=a;
				long endPosition = a+threadlen;
				if(endPosition>=filelen){
					pair.end=filelen;
					map.put(count, pair);
					count++;
					raf.close();
					return;
				}
				raf.seek(endPosition);
				byte tmp =(byte) raf.read();
				while(tmp!='\n' && tmp!='\r'){
					endPosition++;
					if(endPosition>=filelen){
						endPosition=filelen;
						break;
					}
					raf.seek(endPosition);
					tmp =(byte) raf.read();
				}
				pair.end=endPosition;
				//存放位置
				map.put(count, pair);
				count++;
				//递归分段
				sliceTxt(endPosition, threadlen);
	}
	public Map fanhuiMap() {
//		for(int i = 0 ;i<threadsize;i++) {
//			System.out.println(" i "+map.get(i)+" file  "+this.filelen);
//		}
		return map;
	}
}

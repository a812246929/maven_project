package homework.experiment_7;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class LetterCount {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(LetterCount.class.getName());

	public static int countLetters(String s) {
		int strNum = 0;
		int numNum = 0;
		Character[] c = new Character[s.length()];
		for (int i = 0; i < s.length(); i++) {
			c[i] = s.charAt(i);
		}
		for (int i = 0; i < s.length(); i++) {
			for (int j = 0; j <= 9; j++) {
				if (c[i] == j) {
					numNum++;
				}
			}
			for (char j = 'a'; j < 'z'; j++) {
				if (c[i]==j) {
					strNum++;
				}
			}
			for (char j = 'A'; j < 'Z'; j++) {
				if (c[i]==j) {
					strNum++;
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("countLetters(String) - {}", "字符个数：" + strNum); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug("countLetters(String) - {}", "数字个数：" + numNum); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return strNum;
	}
}



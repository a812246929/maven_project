package homework.experiment_7;

public class B_to_D {
	public static int parseBinary(String binaryString) {
		int x = 0;
		for(char c: binaryString.toCharArray())
		x = x * 2 + (c == '1' ? 1 : 0);
		return x;
	}
}


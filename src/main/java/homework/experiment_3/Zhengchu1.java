package homework.experiment_3;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Zhengchu1 {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Zhengchu1.class.getName());

	public static void main(String[] args) {
		int n=0;
		for(int i=100;i<=1000;i++)
		{
			if (i % 5 == 0 && i % 6 == 0) {
				if (logger.isDebugEnabled()) {
					logger.debug(i + " "); //$NON-NLS-1$ //$NON-NLS-2$
				}
				n++;
			}
			if(n==10)
			{
				if (logger.isDebugEnabled()) {
					logger.debug( ""); //$NON-NLS-1$ //$NON-NLS-2$
				}
				n=0;
			}
		}
	}
}

package homework.experiment_3;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Xunhuan {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Xunhuan.class.getName());

	public static void main(String[] args) {
		for (int i = 1; i < 7; i++) {
			for (int j = 1; j <= 7 - i; j++) {
				if (logger.isDebugEnabled()) {
					logger.debug(j); // $NON-NLS-1$
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug(""); //$NON-NLS-1$
			}
		}
	}
}

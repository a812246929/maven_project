package homework.experiment_3;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

public class Paixu {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Paixu.class.getName());

public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	int[] a = new int[3];
	for(int i =0;i<3;i++) {
	a[i] = sc.nextInt();
	}
	for(int i = 1;i<3;i++) {
		for(int j = 0;j<3-i;j++) {
			int temp;
			if(a[j]>a[j+1]) {
			temp = a[j];
			a[j]=a[j+1];
			a[j+1]=temp;
			}
		}
	}
	for(int i = 0;i<a.length;i++)
	{
			if (logger.isDebugEnabled()) {
				logger.debug(a[i] + " "); //$NON-NLS-1$ //$NON-NLS-2$
			}
	}
}
}

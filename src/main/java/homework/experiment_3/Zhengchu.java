package homework.experiment_3;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

public class Zhengchu {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Zhengchu.class.getName());

public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	int i = sc.nextInt();
	if(i%5==0&&i%6==0) {
			if (logger.isDebugEnabled()) {
				logger.debug(i + " is divisible by both 5 and 6"); //$NON-NLS-1$ //$NON-NLS-2$
			}
	}else
		if(i%5==0||i%6==0) {
			if (logger.isDebugEnabled()) {
				logger.debug( i + " is divisible by 5 or 6, but not both"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}else
		if (logger.isDebugEnabled()) {
			logger.debug( i + " is not divisible by either 5 or 6"); //$NON-NLS-1$ //$NON-NLS-2$
		}
}
}

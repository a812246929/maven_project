package homework.experiment_0;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Experiment_0 {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Experiment_0.class.getName());
	
	/**
	 *    实验一
	 */
	public void welcome() {
		if (logger.isDebugEnabled()) {
			logger.debug("Welcome to Java!"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
	
	public void welcomeInMessageDialogBox() {
		JOptionPane.showMessageDialog(null, "Welcome to Java!", "Display Message", JOptionPane.INFORMATION_MESSAGE);
	}
	/**
	 *    实验二
	 */
	//圆柱体体积
	public void cylinder() {
		final double π = 3.1415;

		double area, radius = 0, volume, length = 0;

		String s1 = JOptionPane.showInputDialog("", "请输入圆柱体的半径：");
		try {
			int i = Integer.parseInt(s1);
			radius = i;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("输入的数据类型不对，程序将退出"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			System.exit(0);
		}

		String s2 = JOptionPane.showInputDialog("", "请输入圆柱体的高：");
		try {
			int l = Integer.parseInt(s2);
			length = l;
		} catch (Exception e1) {
			if (logger.isDebugEnabled()) {
				logger.debug("输入的数据类型不对，程序将退出"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			System.exit(0);
		}
		area = radius * radius * π;
		volume = area * length;
		if (logger.isDebugEnabled()) {
			logger.debug(volume); //$NON-NLS-1$
		}
	}
	//显示整数的Ascii码
	public char num_to_abc(int i) {
//		Scanner sc = new Scanner(System.in);
//		if (logger.isDebugEnabled()) {
//			logger.debug("请输入0-127之间的数"); //$NON-NLS-1$ //$NON-NLS-2$
//		}
//		int i;
//		i = sc.nextInt();
		if (i >= 0 && i <= 127) {
			char n = (char) i;
			if (logger.isDebugEnabled()) {
				logger.debug( n); //$NON-NLS-1$
				return n;
			}
		} else
		if (logger.isDebugEnabled()) {
			logger.debug("输入错误"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return '0';
	}
	//转换大小写
	public char letterA_to_a(char cc) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("请输入大写字母："); //$NON-NLS-1$ //$NON-NLS-2$
//		}
//		Scanner sc = new Scanner(System.in);
//		char cc = sc.next().charAt(0);
		int a = (int) cc;
		if (a >= 65 && a <= 90) {
			if (logger.isDebugEnabled()) {
				logger.debug( cc + " to " + (char) (a + 32)); //$NON-NLS-1$ //$NON-NLS-2$
			}
			return (char) (a + 32);
		} else
		if (logger.isDebugEnabled()) {
			logger.debug("输入错误"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return '0';
	}
	//数字各位之和
	public int  nsum(int n) {
//		if (logger.isDebugEnabled()) {
//			logger.debug("输入一个3位数"); //$NON-NLS-1$ //$NON-NLS-2$
//		}
//		Scanner sc = new Scanner(System.in);
//		int n = sc.nextInt();
		int a, b, c;
		a = n / 100;
		b = (n % 100) / 10;
		c = (n % 100) % 10;
		if (logger.isDebugEnabled()) {
			logger.debug(a +""+b +""+c + "=" + (a + b + c)); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return  (a + b + c);
	}
	/**
	 * 实验三
	 */
	//排序
	public int[] paixu(int[] a) {
//		Scanner sc = new Scanner(System.in);
//		int[] a = new int[3];
//		for (int i = 0; i < 3; i++) {
//			a[i] = sc.nextInt();
//		}
		for (int i = 1; i < 3; i++) {
			for (int j = 0; j < 3 - i; j++) {
				int temp;
				if (a[j] > a[j + 1]) {
					temp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = temp;
				}
			}
		}
		for (int i = 0; i < a.length; i++) {
			if (logger.isDebugEnabled()) {
				logger.debug(a[i] + " "); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return a;
	}
	//判断整除
	public int zhengchu(int i) {
//		Scanner sc = new Scanner(System.in);
//		int i = sc.nextInt();
		if (i % 5 == 0 && i % 6 == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug(i + " is divisible by both 5 and 6"); //$NON-NLS-1$ //$NON-NLS-2$
				return 1;
			}
		} else if (i % 5 == 0 || i % 6 == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug( i + " is divisible by 5 or 6, but not both"); //$NON-NLS-1$ //$NON-NLS-2$
				return 2;
			}
		} else
		if (logger.isDebugEnabled()) {
			logger.debug( i + " is not divisible by either 5 or 6"); //$NON-NLS-1$ //$NON-NLS-2$
			return 3;
		}
		return 0;
	}
	//判断整除1
	public void Zhengchu1() {
		int n = 0;
		for (int i = 100; i <= 1000; i++) {
			if (i % 5 == 0 && i % 6 == 0) {
				if (logger.isDebugEnabled()) {
					logger.debug( i + " "); //$NON-NLS-1$ //$NON-NLS-2$
				}
				n++;
			}
			if (n == 10) {
				if (logger.isDebugEnabled()) {
					logger.debug( ""); //$NON-NLS-1$ //$NON-NLS-2$
				}
				n = 0;
			}
		}
	}
	//循环输出
	public void xunhuan() {
		for (int i = 1; i < 7; i++) {
			for (int j = 1; j <= 7 - i; j++) {
				if (logger.isDebugEnabled()) {
					logger.debug( j); //$NON-NLS-1$
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug( ""); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}

	}
	//分数求和
	public String fenmuQiuhe() {
		double sum = 0;
		for (double i = 1; i < 100;) {
			sum += (i / (i + 2));
			i = i + 2;
		}
		if (logger.isDebugEnabled()) {
			logger.debug( String.format("%.2f", sum)); //$NON-NLS-1$ //$NON-NLS-2$
			return String.format("%.2f", sum);
		}
		return " ";
	}
	/**
	 * 实验四
	 */
	//数字各位之和
	public static int sumDigits(long n) {
		long a,b,c,d,e;
		a=n/10000;
		b=(n%10000)/1000;
		c=(n%10000)%1000/100;
		d=(n%10000)%1000%100/10;
		e=(n%10000)%1000%100%10;
		return (int)(a+b+c+d+e);
	}

}

package homework.experiment_8;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Employee extends Person{
	Calendar hireDay;
	double salary;
	public Employee() {
	}
	Employee(String name ,double salary,int year,int month,int day){
		this.name = name;
		this.salary = salary;
		hireDay = new  GregorianCalendar();
		this.hireDay.set(year, month-1, day);
	}
	
	public String getDescription() {
		return "Employee";
	}
	
	public Date getDate() {
		return this.hireDay.getTime();
	}
	
	public double getSalary() {
		return this.salary;
	}
	@Override
	public String toString() {
		return this.getDescription()+"[name="+this.getName()+", salary="+this.getSalary()+",hireDay="+this.getDate()+"]";
	}
}


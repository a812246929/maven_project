package homework.experiment_8;

import java.util.GregorianCalendar;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Manager extends Employee{
	double bonus;
	public Manager() {
		this.bonus = 0;
	}
	Manager(String name ,double salary,int year,int month,int day){
		super(name,salary,year,month,day);
		this.bonus = 0;
	}
	
	public String getDescription() {
		return "Manager";
	}

	public double getSalary() {
		return this.salary+this.bonus;
	}
	
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	@Override
	public String toString() {
		return this.getDescription()+"[name="+this.getName()+", salary="+this.getSalary()+",hireDay="+this.getDate()+"]"+"[bonus="+this.bonus+"]";
	}
}

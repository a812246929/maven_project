package homework.experiment_2;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.awt.*;

import javax.swing.*;

public class Cylinder {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Cylinder.class.getName());

	private static final double π = 3.1415;

	public static void main(String[] args) {
		double area,radius = 0,volume,length = 0;
		
		String s1=JOptionPane.showInputDialog("","请输入圆柱体的半径：");
		try{int i =Integer.parseInt(s1); 
		radius = i;
		}
		catch(Exception e) 
		{
			if (logger.isDebugEnabled()) {
				logger.debug("输入的数据类型不对，程序将退出"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		System.exit(0); 
		}
		
		String s2=JOptionPane.showInputDialog("","请输入圆柱体的高：");
		try{int l=Integer.parseInt(s2); 
		length = l;
		}
		catch(Exception e1) 
		{
			if (logger.isDebugEnabled()) {
				logger.debug("输入的数据类型不对，程序将退出"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		System.exit(0); 
		}
		area = radius * radius * π;
		volume = area * length;
		if (logger.isDebugEnabled()) {
			logger.debug(volume); //$NON-NLS-1$
		}
	}
}
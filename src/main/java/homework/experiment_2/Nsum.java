package homework.experiment_2;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

public class Nsum {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Nsum.class.getName());

	public static void main(String[] args) {
		if (logger.isDebugEnabled()) {
			logger.debug("输入一个3位数"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a,b,c;
		a=n/100;
		b=(n%100)/10;
		c=(n%100)%10;
		if (logger.isDebugEnabled()) {
			logger.debug( a + +b + +c + "=" + (a + b + c)); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
}

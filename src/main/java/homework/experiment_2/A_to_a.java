package homework.experiment_2;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

/**
 * @author Administrator
 *
 */
public class A_to_a {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(A_to_a.class.getName());

	public static void main(String[] args) {
		if (logger.isDebugEnabled()) {
			logger.debug("请输入大写字母："); //$NON-NLS-1$ //$NON-NLS-2$
		}
		Scanner sc = new Scanner(System.in);
		char cc = sc.next().charAt(0);
		int a = (int) cc;
		if (a >= 65 && a <= 90) {
			if (logger.isDebugEnabled()) {
				logger.debug(cc + " to " + (char) (a + 32)); //$NON-NLS-1$ //$NON-NLS-2$
			}
		} else
		if (logger.isDebugEnabled()) {
			logger.debug("输入错误"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
}

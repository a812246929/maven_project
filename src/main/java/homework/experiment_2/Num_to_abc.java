package homework.experiment_2;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

public class Num_to_abc {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Num_to_abc.class.getName());

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		if (logger.isDebugEnabled()) {
			logger.debug("请输入0-127之间的数"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		int i ;
		i = sc.nextInt();
		if(i>=0&&i<=127) {
			char n = (char)i;
			if (logger.isDebugEnabled()) {
				logger.debug( n); //$NON-NLS-1$
			}
		}
		else
		if (logger.isDebugEnabled()) {
			logger.debug("输入错误"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
}

package homework.experiment_6;

public class Vote {
	int count;
	Vote(){
		count = 0;
	};
	int getCount() {
		return count;
	}
	void setCount(int Count) {
		this.count = Count;
	}
	
	void clean() {
		count = 0;
	}
	void increment() {
		count++;
	}
	void decrement() {
		count--;
	}
}

package homework.experiment_6;

public class TestFan {
	public static void main(String[] args) {
		Fan testFan = new Fan();
		//颜色
		String Color = "yellow";
		testFan.setColor(Color);
		//状态
		boolean On = true;
		testFan.setOn(On);
		//风速
		int Speed = 100;
		testFan.setSpeed(Speed);
		//半径
		double Radius = 35;
		testFan.setRadius(Radius);
		
		System.out.println("颜色："+testFan.getColor());
		System.out.println("状态："+testFan.isOn());
		System.out.println("速度："+testFan.getSpeed());
		System.out.println("半径："+testFan.getRadius());
		
		System.out.println("风扇信息：\n"+testFan.toString());
		
	}
}

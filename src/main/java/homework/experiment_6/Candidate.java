package homework.experiment_6;

public class Candidate extends CandidateBase {
	String name;
	Vote vote = new Vote();
	
	Candidate(){
		}
	
	Candidate(String name,int count){
		super();
		this.name = name;
		this.vote.count = count;
		numberOfCandidates++;
	}
	
	String getName() {
		return name;
	}
	Vote getVote() {
		return vote;
	}
	int getNUmbleOfCandidates() {
		return numberOfCandidates;
	}
}

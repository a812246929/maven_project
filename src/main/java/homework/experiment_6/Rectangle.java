package homework.experiment_6;

public class Rectangle {
	double width;
	double height;
	String color;
	
	Rectangle(){}
	Rectangle(double width,double height ,String color){
		this.width = width;
		this.height = height;
		this.color = color;
	}
	double getWidth() {
		return width;
	}
	void setWidth(double width) {
		this.width = width;
		}
	
	double getHeight() {
		return height;
	}
	void setHeight(double height) {
		this.height = height;
	}
	
	String getColor() {
		return color;
	}
	void setColor(String color) {
		this.color = color;
	}
	double findArea() {
		return width*height;
	}
	double findPerimenter() {
		return (width+height)*2;
	}
}

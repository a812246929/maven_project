package homework.experiment_6;

public class MyPoint {
	double x;
	double y;
	MyPoint(){
		x = 0;
		y = 0;
	}
	MyPoint(double x,double y){
		this.x = x;
		this.y = y;
	}
	double getX() {
		return x;
	}
	double getY() {
		return y;
	}
	double distance(MyPoint secondPoint) {
		return Math.sqrt((secondPoint.x-x)*(secondPoint.x-x)+(secondPoint.y-y)*(secondPoint.y-y));
	}
	double distance(MyPoint p1,MyPoint p2) {
		return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
	}
}

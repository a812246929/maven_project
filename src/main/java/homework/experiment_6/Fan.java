package homework.experiment_6;

public class Fan {
	public int speed;
	public double radius;
	public boolean on;
	public String color;
	
	public Fan() {
	}
	//速度
	public int getSpeed() 
	{
		return speed;
	}
	public void setSpeed(int Speed) 
	{
		this.speed = Speed;
	}
	//开关状态
	public boolean isOn() {
		return on;
	}
	public void setOn(boolean On) {
		this.on = On;
	}
	//半径
	public double getRadius() {
		return radius;
	}
	public void setRadius(double Radius) {
		this.radius = Radius;
	}
	//颜色
	public String getColor() {
		return color;
	}
	public void setColor(String Color) {
		this.color = Color;
	}
	//输出信息
	public String toString() {
		String a = "风扇颜色："+color+"\n风扇状态："+on+"\n风扇速度："+speed+"\n风扇半径："+radius;
		return a;
	}
}

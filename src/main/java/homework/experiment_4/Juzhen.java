package homework.experiment_4;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Juzhen {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Juzhen.class.getName());

	public static void printMatrix(int n) {
		for(int i=0;i<n;i++) {
			for(int j=0;j<n;j++) {
				if (logger.isDebugEnabled()) {
					logger.debug(1); //$NON-NLS-1$
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug(""); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}
	public static void main(String[] args) {
		printMatrix(3);
	}
}



package homework.experiment_4;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Zhuanhuan {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Zhuanhuan.class.getName());

	/** Converts from feet(英尺) to meters(米) */
	public static double footToMeter(double foot) {
		return (0.3048*foot);
	}

	/** Converts from meters to feet */
	public static double meterToFoot(double meter) {
		return (3.2808*meter);
	}
	public static void main(String[] args) {
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - {}", "Feet          Meter      |  Meter          Feet"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		for(double i=1;i<11;i++) {
			System.out.printf("%.1f           %.3f      |  ",i,footToMeter(i));
			System.out.printf("%.1f          %f\n",i*5+15,meterToFoot(i));
		}
	}
}

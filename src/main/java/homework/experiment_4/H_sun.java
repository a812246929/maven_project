package homework.experiment_4;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

public class H_sun {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(H_sun.class.getName());

	public static int sumDigits(long n) {
		long a,b,c,d,e;
		a=n/10000;
		b=(n%10000)/1000;
		c=(n%10000)%1000/100;
		d=(n%10000)%1000%100/10;
		e=(n%10000)%1000%100%10;
		return (int)(a+b+c+d+e);
	}
	public static void main(String[] args) {
		if (logger.isDebugEnabled()) {
			logger.debug( "请输入一个五位数"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		if (logger.isDebugEnabled()) {
			logger.debug( sumDigits(n)); //$NON-NLS-1$
		}
		
	}
}

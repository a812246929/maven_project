package homework.experiment_4;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Show_atob {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Show_atob.class.getName());

	public static void printChars(char ch1, char ch2, int numberPerLine) {
		int a = (int)ch1;
		int b = (int)ch2;
		int num=0;
		for(int i=ch1;i<=ch2;i++) {
			if (logger.isDebugEnabled()) {
				logger.debug( (char) i); //$NON-NLS-1$
			}
			num+=1;
			if(num ==numberPerLine) {
				if (logger.isDebugEnabled()) {
					logger.debug(""); //$NON-NLS-1$ //$NON-NLS-2$
				}
				num=0;
			}
		}
	}
	public static void main(String[] args) {
		char ch1='1',ch2='Z';
		printChars(ch1,ch2,10);
	}
}

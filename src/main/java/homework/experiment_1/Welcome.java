package homework.experiment_1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author liruilin
 *
 */
public class Welcome {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Welcome.class.getName());

	public static void main(String[] args) {
		logger.error("Welcome to Java!");
		if (logger.isDebugEnabled()) {
			logger.debug("main(String[]) - {}", "Welcome to Java!"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
}

package homework.experiment_1;

import javax.swing.JOptionPane;

/**
 * @author liruilin
 *
 */
public class WelcomeInMessageDialogBox {
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, "Welcome to Java!", "Display Message", JOptionPane.INFORMATION_MESSAGE);
	}
}
package homework.experiment_9;

public class StudentManagerImpl implements StudentManager {
	
	@Override
	public void printList(Student[] students) {
		for(Student s :students)
		{
			System.out.println(s);
		}
	}

	@Override
	public Student max(Student[] students) {
		Student studentTemp = students[0];
		for(Student s :students)
		{
			System.out.println(studentTemp.compareTo(s));
			if(studentTemp.compareTo(s) == 1)
				studentTemp = s;
		}
		return studentTemp;
	}

	@Override
	public void sort(Student[] students) {
		for(int i = 1;i<students.length;i++)
		{
			for (int j =0;j<students.length-i;j++)
			{
				if(students[j].compareTo(students[j+1]) < 0)
				{	
					Student studentTemp=students[j];
					students[j] = students[j+1];
					students[j+1] = studentTemp;
				}
			}
		}

	}

}

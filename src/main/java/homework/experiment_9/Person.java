package homework.experiment_9;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public  class Person implements Comparable <Person>{
	Name name;
	public Person() {
	}
	public Person(Name name){
		this.name = name;
	}
	public Name getName() {
		return name;
	}
	public void setName(Name name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return name.getFullName()+"\n";
	}
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	@Override
	public int compareTo(Person person) {
		return this.name.firstName.compareTo(person.name.firstName);
	}

}

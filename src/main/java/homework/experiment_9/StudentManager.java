package homework.experiment_9;

public interface StudentManager {

	public void printList(Student[] students);
	public Student max(Student[] students);
	public void sort(Student[] students);

}

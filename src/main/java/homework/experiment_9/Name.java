package homework.experiment_9;

public  class Name {
	String firstName;
	String lastName;
	char mi;
	public Name() {

	}
	public Name(String firstName,char mi,String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
		this.mi = mi;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public char getMi() {
		return mi;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setMi(char mi) {
		this.mi = mi;
	}
	
	public String getFullName(){
		return firstName+" "+mi+" "+lastName;
	}
}

package homework.experiment_9;

import javax.print.attribute.standard.RequestingUserName;

public  class Student extends Person  {
	String major;
	public Student() {
	}
	public Student(Name name) {
		super(name);
		this.major = "Computer Science";
	}
	Student(Name name,String major){
		super(name);
		this.major = major;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	@Override
	public String toString() {
		return name.getFullName()+"\nMajor: "+this.major;
	}  
	public int compareTo(Student student ) {
		if(this.major.compareTo(student.major)>0)
			return -1;
		if(this.major.compareTo(student.major)==0)
			{
			if(this.name.lastName.compareTo(student.name.lastName)>0)
				return -1;
			if(this.name.lastName.compareTo(student.name.lastName) == 0)
				{
				if (this.name.firstName.compareTo(student.name.firstName)>0)
					return -1;
				if (this.name.firstName.compareTo(student.name.firstName)==0)
				{
					if(this.name.mi>student.name.mi)
						return -1;
					if(this.name.mi == student.name.mi)
						return 0;
					if(this.name.mi < student.name.mi)
						return 1;
				}
				if (this.name.firstName.compareTo(student.name.firstName)<0)
					return 1;
				}
			if(this.name.lastName.compareTo(student.name.lastName) < 0)
				return 1;
			}
		if (this.major.compareTo(student.major)<0)
			return 1;
		return 0;
	}


	
}

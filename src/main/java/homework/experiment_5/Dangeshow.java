package homework.experiment_5;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

public class Dangeshow {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Dangeshow.class.getName());

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] b = { 1, 2, 2, 3, 3, 2, 2, 1, 1, 3 };
		int i, t, p = 0;
		for (i = 0; i < 10; i++) {
			for (t = 0; t < i; t++)
				if (b[t] == b[i])
					break;
			if (t == i)
				if (logger.isDebugEnabled()) {
					logger.debug(b[i]); //$NON-NLS-1$
				}
		}
	}
}

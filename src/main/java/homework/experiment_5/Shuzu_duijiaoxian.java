package homework.experiment_5;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Shuzu_duijiaoxian {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Shuzu_duijiaoxian.class.getName());

	public static void main(String[] args) {
		int[][] a = new int[5][5];
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = (i + 1) * (j + 1);
				if (logger.isDebugEnabled()) {
					logger.debug( a[i][j] + " "); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug(""); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		for (int i = 0; i < a.length; i++) {
			sum += a[i][i];
		}
		if (logger.isDebugEnabled()) {
			logger.debug(sum); //$NON-NLS-1$
		}
	}
}

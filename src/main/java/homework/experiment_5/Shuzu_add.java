package homework.experiment_5;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Shuzu_add {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Shuzu_add.class.getName());

	public static int[][] addMatrix(int[][] m1, int[][] m2) {
		int[][] m = m1;
		for (int i = 0; i < m1.length; i++) {
			for (int j = 0; j < m1[i].length; j++) {
				m[i][j] = m1[i][j] + m2[i][j];
			}
		}
		return m;
	}

	public static void main(String[] args) {

		int[][] m1 = { { 1, 8, 5, 9, 3 }, { 4, 0, 0, 9, 8 }, { 9, 1, 3, 7, 3 }, { 1, 3, 4, 6, 4 }, { 9, 5, 1, 1, 2 } };
		int[][] m2 = { { 7, 6, 7, 9, 0 }, { 6, 3, 8, 8, 1 }, { 0, 0, 6, 1, 6 }, { 7, 4, 3, 8, 6 }, { 2, 4, 6, 3, 1 } };
		int[][] m = addMatrix(m1, m2);
		for (int i = 0; i < m1.length; i++) {
			for (int j = 0; j < m1[i].length; j++) {
				if (logger.isDebugEnabled()) {
					logger.debug(m[i][j] + " "); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug(""); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}
}

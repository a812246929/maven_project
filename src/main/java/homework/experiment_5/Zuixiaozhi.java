package homework.experiment_5;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Zuixiaozhi {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Zuixiaozhi.class.getName());

	public static int minIndex(int[] list) {
		int a = list[0];
//		int b =0;		//可以用来记录最小值再数组的那个位置
		for(int i=1;i<list.length;i++) {
			if(a>list[i]){
				a = list[i];
//				b = i;
			}
		}
		return a;
	}
	public static void main(String[] args) {
		int[] a= {10,9,8,7,6,5,4,3,2,1};
		if (logger.isDebugEnabled()) {
			logger.debug("最小值为：" + minIndex(a)); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
}

package test;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import InputExcel.Achievement;
import dao.mysqldao.MySQLDao;
import dao.mysqldao.impl.MySQLDaoImpl;

public class jsondemo {
	public static void main(String[] args) {
		MySQLDao my = new MySQLDaoImpl();
		List<Achievement> list = my.outMySQL();
//		JSONArray j = new JSONArray();
//		for(Achievement a :list) {
//			String jsonObject = JSON.toJSONString(a);
//			j.add(jsonObject);
//		}
//		String jsonObject1 = j.toJSONString();
//		System.out.println(j);
//		System.out.println(jsonObject1);
		
		String jsonObject = JSON.toJSONString(list);
//		System.out.println(jsonObject);
		JSONArray j1 = new JSONArray();
		List<Achievement> list2=j1.parseArray(jsonObject, Achievement.class);
		for(Achievement i :list2) {
			Achievement o = i;
			System.out.println(o);
		}
		
//		List<Achievement> list1 = (List<Achievement>) JSON.parseObject(jsonObject);
//		System.out.println(list1);
//		JSONArray j = new JSONArray();
//		for(Achievement a :list) {
//			JSONObject jo = new JSONObject();
//			jo = JSON.toJSONString(list);
//		}
	}
}

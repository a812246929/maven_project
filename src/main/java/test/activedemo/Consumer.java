package test.activedemo;

import java.io.IOException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
 
/**
 * 消息消费者(consumer):它用于接收发送到目的地的消息
 */
public class Consumer {
 
	public static final String url = "tcp://localhost:61616"; // 缺省端口,如果要改,可在apache-activemq-5.13.3\conf中的activemq.xml中更改端口号
	ConnectionFactory factory;
	Connection connection;
	Session session;
	MessageConsumer[] consumers;
	ComunicateMode comunicateMode = ComunicateMode.pubsub;
	
	enum ComunicateMode {
		p2p, pubsub
	}
	public Consumer(ComunicateMode mode, String[] destinationNames) throws JMSException{
		this.comunicateMode = mode;
		factory = new ActiveMQConnectionFactory(url); // 这里的url也可以不指定,java代码将默认将端口赋值为61616
		connection = factory.createConnection();
		connection.start();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		consumers = new MessageConsumer[destinationNames.length];
		
		for (int i = 0; i < destinationNames.length; i++) {
			Destination destination = comunicateMode == ComunicateMode.pubsub ? session.createTopic("Topic." + destinationNames[i]) : session.createQueue("Queue." + destinationNames[i]);
			consumers[i] = session.createConsumer(destination);
			consumers[i].setMessageListener(new MessageListener(){//监听
				public void onMessage(Message message) {//处理接受的消息
					try {
						System.out.println(String.format("收到消息[%s]", ((TextMessage) message).getText()));
					} catch (JMSException e) {
						e.printStackTrace();
					}
				}
			});
		}
	}
	
	public void close() throws JMSException {
		if(connection != null) {
			connection.close();
		}
	}
	
	public static void main(String[] args) throws JMSException, IOException {
		Consumer consumer = new Consumer(ComunicateMode.p2p,new String[] {"2"}); // 这里可以修改消息传输方式为pubsub
		System.in.read();
		consumer.close();
	}
}

package test.activedemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
/**
 * 消息生产者(produceer):用于把消息发送到一个目的地
 */
public class Publisher {
 
	public  static final  String url = "tcp://localhost:61616"; // 缺省端口,如果要改,可在activemq.xml中更改端口号
	
	ConnectionFactory factory;
	Connection connection;
	Session session;
	MessageProducer producer;
	Destination[] destinations;
	ComunicateMode comunicateMode = ComunicateMode.pubsub;
	
	enum ComunicateMode { //枚举类型
		p2p, pubsub
	}
	
	public Publisher(ComunicateMode mode) throws JMSException {
		this.comunicateMode = mode;
		factory = new ActiveMQConnectionFactory(url); // 这里的url也可以不指定,java代码将默认将端口赋值为61616
		connection = factory.createConnection();
		try {
			connection.start();
		} catch (JMSException e){
			connection.close();
			throw e;
		}
		//Session 提供了事务的功能,，第一个参数是是否支持事务，第二个是事务的类型
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		producer = session.createProducer(null);
	}
	protected void setDestinations(String[] stocks) throws JMSException { //设置消息被发送的目的地和客户端消息的来源
		destinations = new Destination[stocks.length];
		for(int i = 0; i < stocks.length; i++) {
			//
			destinations[i] = comunicateMode == ComunicateMode.pubsub ? session.createTopic("Topic." + stocks[i]) : session.createQueue("Queue." + stocks[i]);
		}
	}
	protected void sendMessage(String msg) throws JMSException {
		for (Destination item : destinations) {
			TextMessage msgMessage = session.createTextMessage(msg);
			producer.send(item, msgMessage);
			System.out.println(String.format("成功向Topic为[%s]发送消息[%s]", item.toString(), msgMessage.getText()));
		}
	}
	protected void close() throws JMSException {
		if(connection != null) {
			connection.close();
		}
	}
	
	public static void main(String[] args) throws JMSException, InterruptedException, IOException {
		Publisher publisher = new Publisher(ComunicateMode.p2p); // 这里可以修改消息传输方式为pubsub
		publisher.setDestinations(new String[] {"1","2","3"});
		BufferedReader reader = null;
		String contentString = "";
		do {
			System.out.println("请输入要发送的内容(exit退出)");
			reader = new BufferedReader(new InputStreamReader(System.in));
			contentString = reader.readLine();
			if(contentString.equals("exit")) {
				break;
			}
			publisher.sendMessage(contentString);
		}while (!contentString.equals("exit"));
		reader.close();
		publisher.close();
	}
}
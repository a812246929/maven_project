package morethread;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import basedemo.Student;
import dao.mysqldao.MySQLDao;
import dao.mysqldao.impl.MySQLDaoImpl;

public class Main {
	public static void main(String[] args) throws IOException, InterruptedException {
		String filename = "E:/159.txt";
		RandomAccessFile raf = new RandomAccessFile(filename,"r");
		int threadsize = 10;
		long start = 3;
		long end =raf.length();
		long len = end - start;
		System.out.println("end"+end+" len"+len);
		raf.seek(3);
		System.out.println("开始时指针的位置"+raf.getFilePointer());
		System.out.println(len);
		//i 是一个线程读取多少字节
		long i = len/threadsize;
		System.out.println("i "+i);
		for(int j =0;j<threadsize;j++) {
			if(j == threadsize-1) {
				Thread thread = new Thread(new ReadTxt(start, end, filename,"r"),"a"+j);
				thread.start();
				break;
			}
			Thread thread = new Thread(new ReadTxt(start, start+i, filename,"r"),"a"+j);
			thread.start();
			start = start+i;
		}	
		StringBuffer sb = new StringBuffer();
		for(int j = 0;j<threadsize;j++) {
			sb.append(ReadTxt.map.get("a"+j));
		}
	}
	
	public static void importtxt(StringBuffer sb) {
		Connection conn = null;
		PreparedStatement pst = null; // 继承类 继承自Statement
		String URL = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
		String USER = "root";
		String PASSWORD = "lilin123";
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			String s = "insert into t_txt(NAME) values(?)";
			pst = conn.prepareStatement(s);
			String s1 = sb.toString();
			pst.setString(1, s1);
			pst.execute();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

package morethread.threaddemo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class MainDemo {
	public static void main(String[] args) throws IOException, InterruptedException {
		String filename = "E:/789.txt";
		RandomAccessFile raf = new RandomAccessFile(filename,"r");
		int threadsize = 5;
		long start = 3;
		long end =raf.length();
		long len = end - start;
		System.out.println("end"+end+" len"+len);
		raf.seek(3);
		System.out.println("开始时指针的位置"+raf.getFilePointer());
		System.out.println(len);
		//i 是一个线程读取多少字节
		long i = len/threadsize;
		System.out.println("i "+i);
		for(int j =0;j<threadsize;j++) {
			if(j == threadsize-1) {
				Thread thread = new Thread(new ReadTxtdemo(start, end, filename,"r"),"a"+j);
				thread.start();
				break;
			}
			Thread thread = new Thread(new ReadTxtdemo(start, start+i, filename,"r"),"a"+j);
			thread.start();
			start = start+i;
		}	
		Thread.sleep(10);
		StringBuffer sb = new StringBuffer();
		for(int j = 0;j<threadsize;j++) {
			System.out.println("a"+j+" "+ReadTxtdemo.map.get("a"+j));
			sb.append(ReadTxtdemo.map.get("a"+j));
		}
		System.out.println("sb  "+sb.toString());
		byte[] bt =sb.toString().getBytes();
		String s = new String(bt,"UTF-8");
		System.out.println(s);
	}
}


package morethread.threaddemo;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

public class ReadTxtdemo implements Runnable {

	//定义字节数组（取水的竹筒）的长度    
    private final int BUFF_LEN = 300;    
    //定义读取的起始点    
    long start;    
    //定义读取的结束点    
    long end;   
    //将读取到的字节输出到raf中  randomAccessFile可以理解为文件流，即文件中提取指定的一部分的包装对象  
	RandomAccessFile raf;
	//
	static Map<String,StringBuffer> map = new HashMap();
	
//	public ReadTxt(long start, long end, RandomAccessFile raf,String s1,String s2) {
//		this.start = start;
//		this.end = end;
//		this.raf = raf;
//	}
	public ReadTxtdemo(long start, long end,String s1,String s2) throws FileNotFoundException {
		this.start = start;
		this.end = end;
		this.raf = new RandomAccessFile(s1,s2);
	}

	@Override
	public  void run() {
		byte[] bt = new byte[BUFF_LEN];
		long starttemp = start;
		//每次读取的长度
		long len = end - start;
		//i 是循环的次数 需要读取几次
		long i = len/BUFF_LEN;
		System.out.println("Thread.currentThread().getName() "+Thread.currentThread().getName()+" "+start +" "+ end);
		System.out.println("Thread.currentThread().getName() "+Thread.currentThread().getName()+" "+i);
		StringBuffer sb = new StringBuffer();
		try {
			System.out.println(Thread.currentThread().getName()+" "+"当前指针的位置"+raf.getFilePointer());
			raf.seek(start);
			System.out.println(Thread.currentThread().getName()+" "+"seek后指针的位置"+raf.getFilePointer());
			
			for(int j = 0;j<i ;j++) {
			raf.read(bt);
			String string = new String(bt,"UTF-8");
			sb.append(string);
			starttemp = starttemp+BUFF_LEN;
			raf.seek(starttemp);
			}
			System.out.println(Thread.currentThread().getName()+" start+(i*len) "+(start+(i*len)));
			System.out.println(Thread.currentThread().getName()+" start "+start+" i "+i+" len "+len);
//			raf.seek(start+(i*BUFF_LEN));
			
			System.out.println(Thread.currentThread().getName()+" "+"readfully之前指针的位置"+raf.getFilePointer());
			System.out.println("Thread.currentThread().getName() "+Thread.currentThread().getName() +"  start+i*BUFF_LEN "+(start+i*BUFF_LEN)+" len%BUFF_LEN "+(len%BUFF_LEN));
			String string1 = new String(bt,"UTF-8");
			System.out.println("bt "+ string1+"/");
			byte[] temp = new byte[(int) (len%BUFF_LEN)];
			raf.readFully(temp, 0, (int) (len%BUFF_LEN));
			String string = new String(temp,"UTF-8");
			
			sb.append(string);
			System.out.println("Thread.currentThread().getName() "+Thread.currentThread().getName());
			map.put(Thread.currentThread().getName(), sb);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			raf.close();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

}



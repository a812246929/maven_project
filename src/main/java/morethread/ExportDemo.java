package morethread;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ExportDemo {
	public static void importtxt(StringBuffer sb) {
		Connection conn = null;
		
		PreparedStatement pst = null; // 继承类 继承自Statement
		String URL = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false";
		String USER = "root";
		String PASSWORD = "lilin123";
		try {
			// 1.加载驱动程序
			Class.forName("com.mysql.jdbc.Driver");
			// 2.获得数据库链接
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			// 3.通过数据库的连接操作数据库，实现增删改查（使用Statement类）
			String s = "insert into t_txt(NAME) values(?)";
			pst = conn.prepareStatement(s);
			String s1 = sb.toString();
			pst.setString(1, s1);
			pst.execute();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

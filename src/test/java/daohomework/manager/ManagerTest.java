package daohomework.manager;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import daohomework.eneity.Achievement;
import daohomework.manager.Impl.ManagerImpl;


class ManagerTest {
	private static final Logger logger = LogManager.getLogger(ManagerTest.class.getName());
	Manager manager = new ManagerImpl();
	@Test
	void testAdd() {
		Achievement achievement = new Achievement();
		achievement.setId(12l);
		achievement.setName("杨瑞龙");
		System.out.println();
		assertTrue(manager.add(achievement));
	}

	@Test
	void testRemove() {
		String name = "李瑞林";
		assertTrue(manager.remove(name));	}

	@Test
	void testUpdate() {
		Achievement achievement = new Achievement();
		achievement.setId(12l);
		achievement.setName("杨瑞龙");
		assertTrue(manager.update(achievement));
	}

	@Test
	void testFindByID() {
		Long id = Long.valueOf(5);
		Achievement achievement = new Achievement();
		achievement = this.manager.findByID(id);
		assertNotNull(achievement);
		assertEquals(id, achievement.getId());
		if (logger.isDebugEnabled()) {
			logger.debug(achievement); //$NON-NLS-1$
		}
	}

	@Test
	void testFindByName() {
		List <Achievement> list = new ArrayList<>();
		String name = "李瑞林";
		list = manager.findByName(name);
		for (Achievement achievement :list ){
			if(achievement.getName().equals(name))
				if (logger.isDebugEnabled()) {
					logger.debug(achievement); //$NON-NLS-1$
				}
		}
	}

	@Test
	void testImportexcel() {
		String student_data_file = "/work.xlsx";
		assertTrue(manager.importexcel(student_data_file));
	}

	@Test
	void testImportcsv() {
		String File_Data = "E:\\test.csv";
		assertTrue(manager.importcsv(File_Data));
	}

	@Test
	void testImportjson() {
		String File_Data = "E:\\test.json";
		assertTrue(manager.importjson(File_Data));
	}

	@Test
	void testExportJson() {
		String File_Data = "E:\\test.json";
		assertTrue(manager.exportJson(File_Data));
	}

	@Test
	void testExportExcel() {
		assertTrue(manager.exportExcel("E:\\test.csv"));
	}

	@Test
	void testExportList() {
		List<Achievement> list = new ArrayList();
		assertNotNull(list = manager.exportList());
	}

}

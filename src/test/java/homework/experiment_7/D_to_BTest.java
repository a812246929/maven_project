package homework.experiment_7;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class D_to_BTest {

	@Test
	void testConvertDecimalToBinary() {
		int l= 2801;
		 D_to_B a = new D_to_B();
		 assertEquals(a.convertDecimalToBinary(l),"0000101011110001");
	}

}

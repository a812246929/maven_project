package homework.experiment_7;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class B_to_DTest {

	@Test
	void test() {
		String s = "101011110001"; 
		B_to_D a = new B_to_D();
		assertEquals(a.parseBinary(s),2801);
	}

}

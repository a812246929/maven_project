package homework.experiment_7;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SumTest {

	@Test
	void testCalInteger() {
		int s [] = {1,2,3,4,5,6,7};
		assertEquals(Sum.calInteger(s),28);
	}

}

package homework.experiment_6;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CandidateTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(CandidateTest.class.getName());

	static Candidate cand1 = new Candidate("张三",4);
	static Candidate cand2 = new Candidate("李四",3);

	@Test
	void testGetNUmbleOfCandidates() {
		assertEquals(2, cand1.getNUmbleOfCandidates());
		if (logger.isDebugEnabled()) {
			logger.debug("sum "+cand1.getNUmbleOfCandidates()); //$NON-NLS-1$
		}
	}
	@Test
	void testSetCount() {
		cand1.vote.setCount(4);
		assertEquals(cand1.vote.count,cand1.vote.getCount());
		if (logger.isDebugEnabled()) {
			logger.debug("sum "+cand1.getNUmbleOfCandidates()); //$NON-NLS-1$
		}
	}
	@Test
	void testIncrement() {
		int i =cand1.vote.count;
		cand1.vote.increment();
		assertEquals(cand1.vote.count,i+1);
		if (logger.isDebugEnabled()) {
			logger.debug("count "+cand1.vote.count); //$NON-NLS-1$
		}
	}
	@Test
	void testDecrement() {
		int i =cand1.vote.count;
		cand1.vote.decrement();
		assertEquals(cand1.vote.count,i-1);
		if (logger.isDebugEnabled()) {
			logger.debug("count "+cand1.vote.count); //$NON-NLS-1$
		}
	}
	@Test
	void testClean() {
		cand1.vote.clean();
		assertEquals(cand1.vote.count,0);
		if (logger.isDebugEnabled()) {
			logger.debug("count "+cand1.vote.count); //$NON-NLS-1$
		}
	}
	

}

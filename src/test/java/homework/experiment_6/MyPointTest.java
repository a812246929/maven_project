package homework.experiment_6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyPointTest {
	MyPoint point = new MyPoint(5,6);
	
	@Test
	void testGetX() {
		assertEquals(point.getX(),5);
	}

	@Test
	void testGetY() {
		assertEquals(point.getY(),6);
	}

	@Test
	void testDistanceMyPoint() {
		MyPoint p = new MyPoint(5,7);
		assertEquals(point.distance(p),1);
	}

	@Test
	void testDistanceMyPointMyPoint() {
		MyPoint point1 = new MyPoint(5,5);
		MyPoint point2 = new MyPoint(5,8);
		assertEquals(point.distance(point1,point2),3);
	}

}

package homework.experiment_6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IntTest {
	Int n = new Int(9);
	@Test
	void testGetValue() {
		assertEquals(n.getValue(),9);
	}

	@Test
	void testIsPrime() {
		assertFalse(n.isPrime());
	}

	@Test
	void testIsPrimeInt() {
		assertFalse(n.isPrime(6));
	}

	@Test
	void testIsEven() {
		assertFalse(n.isEven());
	}

	@Test
	void testIsEvenInt() {
		assertTrue(n.isEven(6));
	}

	@Test
	void testEqualsInt() {
		assertTrue(n.equals(9));
	}

}

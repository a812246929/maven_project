package homework.experiment_6;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FanTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(FanTest.class.getName());

	Fan fan;
	
	@Test
	void testFan() {
		this.fan = new Fan();
		fan.setColor("yellow");
		fan.setSpeed(100);
		fan.setRadius(1.5);
		fan.setOn(true);
		if (logger.isDebugEnabled()) {
			logger.debug(this.fan); //$NON-NLS-1$
		}
		assertEquals(fan.getColor(),"yellow");
		assertEquals(fan.getRadius(),1.5);
		assertEquals(fan.getSpeed(),100);
		assertEquals(fan.isOn(),true);
		assertNotNull(this.fan);
	}

}

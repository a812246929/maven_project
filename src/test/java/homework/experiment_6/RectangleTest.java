package homework.experiment_6;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RectangleTest {
	List<Rectangle> list ;
	
	@BeforeEach
	void before() {
		list = new ArrayList<>();
		Rectangle rec1 = new Rectangle();
		rec1.setColor("Blue");
		rec1.setHeight(20);
		rec1.setWidth(10);
		list.add(rec1);
		Rectangle rec2 = new Rectangle(5,10,"yellow");
		list.add(rec2);
	}
	
	@Test
	void testGetWidth() {
			assertEquals(list.get(0).getWidth(),10);
			assertEquals(list.get(1).getWidth(),5);
	}

	@Test
	void testGetHeight() {
		assertEquals(list.get(0).getHeight(),20);
		assertEquals(list.get(1).getHeight(),10);
	}

	@Test
	void testGetColor() {
		assertEquals(list.get(0).getColor(),"Blue");
		assertEquals(list.get(1).getColor(),"yellow");
	}

	@Test
	void testFindArea() {
		assertEquals(list.get(0).findArea(),200);
		assertEquals(list.get(1).findArea(),50);
	}

	@Test
	void testFindPerimenter() {
		assertEquals(list.get(0).findPerimenter(),60);
		assertEquals(list.get(1).findPerimenter(),30);
	}

}

package homework.experiment_8;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ManagerTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ManagerTest.class.getName());

	@Test
	void test() {
		Employee alice1 = new Employee("Alice Adams", 75000, 1987, 12, 15);
		Employee alice2 = alice1;
		Employee alice3 = new Employee("Alice Adams", 75000, 1987, 12, 15);
		Employee bob = new Employee("Bob Brandson", 50000, 1989, 10, 1);

		if (logger.isDebugEnabled()) {
			logger.debug("alice1 == alice2: " + (alice1 == alice2)); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug("alice1 == alice3: " + (alice1 == alice3)); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug("alice1.equals(alice3):" + alice1.equals(alice3)); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug("alice1.equals(bob): " + alice1.equals(bob)); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug("bob.toString(): " + bob); //$NON-NLS-1$ //$NON-NLS-2$
		}

		Manager carl = new Manager("Carl Cracker", 80000, 1987, 12, 15);
		Manager boss = new Manager("Carl Cracker", 80000, 1987, 12, 15);
		boss.setBonus(5000);
		if (logger.isDebugEnabled()) {
			logger.debug( "boss.toString(): " + boss); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug( carl.equals(boss)); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (logger.isDebugEnabled()) {
			logger.debug(alice1.getName() + ", an" + alice1.getDescription() + " with a salary of $" + alice1.getSalary()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (logger.isDebugEnabled()) {
			logger.debug(alice2.getName() + ", an " + alice2.getDescription() + " with a salary of $" + alice2.getSalary()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (logger.isDebugEnabled()) {
			logger.debug(alice3.getName() + ", an " + alice3.getDescription() + " with a salary of $" + alice3.getSalary()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (logger.isDebugEnabled()) {
			logger.debug(bob.getName() + ", an " + bob.getDescription() + " with a salary of $" + bob.getSalary()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (logger.isDebugEnabled()) {
			logger.debug(carl.getName() + ", an " + carl.getDescription() + " with a salary of $" + carl.getSalary()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (logger.isDebugEnabled()) {
			logger.debug(boss.getName() + ", an " + boss.getDescription() + " with a salary of $" + boss.getSalary()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
	}

}

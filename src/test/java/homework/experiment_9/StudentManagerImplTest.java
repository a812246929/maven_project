package homework.experiment_9;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class StudentManagerImplTest {
	StudentManager studentManager = new StudentManagerImpl();
	static Student[] s = new Student[3];
	
	@BeforeAll
	public static void Before() {
	    s[0] = new Student(new Name("Derek", 'S', "Dexony"));
	    s[1] = new Student(new Name("Stacy", 'M', "Waters"));
	    s[2] = new Student(new Name("Adamo", 'U', "Leetmz"));

	}
	
	@Test
	void testPrintList() {
		// Display current array of students
		studentManager.printList(s);
	}
	@Test
	 void  testMax() {
		// Display max of students
	    System.out.println("Max is " + studentManager.max(s));
	    System.out.println();
	}
	@Test
	 void testSort() {
		// Display sorted students
		studentManager.sort(s);
		studentManager.printList(s);
	    System.out.println();
	}
}

package homework.experiment_0;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class experiment_0Test {

	
	Experiment_0  ex ;
	
	@BeforeEach
	void before() {
		this.ex = new Experiment_0();
	}
	
	@Test
	void testWelcome() {
		ex.welcome();
	}

	@Test
	void testWelcomeInMessageDialogBox() {
		ex.welcomeInMessageDialogBox();
	}

	@Ignore
	void testCylinder() {
		ex.cylinder();
	}

	@Test
	void testNum_to_abc() {
		assertEquals(ex.num_to_abc(97),'a');
	}

	@Test
	void testLetterA_to_a() {
		assertEquals(ex.letterA_to_a('A'),'a');
	}

	@Test
	void testNsum() {
		assertEquals(ex.nsum(234),9);
	}

	@Test
	void testPaixu() {
		int[] a = {5,2,9};
		int[] b = {2,5,9};
		assertTrue(Arrays.equals(b, ex.paixu(a)));
	}

	@Test
	void testZhengchu() {
		int a = 9;
		assertEquals(ex.zhengchu(a),3);
	}

	@Test
	void testZhengchu1() {
		ex.Zhengchu1();
	}

	@Test
	void testXunhuan() {
		ex.xunhuan();
	}

	@Test
	void testFenmuQiuhe() {
		assertEquals(ex.fenmuQiuhe(),"46.10");
	}

	@Test
	void testSumDigits() {
		int a = 12345;
		assertEquals(ex.sumDigits(a),15);
	}

}

package bigfilereader.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.Test;

import bigfilereader.test.impl.ReadBigFileImpl;

class ReadBigFileTest {
	ReadBigFile rbf = new ReadBigFileImpl();
	@Test
	void testReadBigFile() {
		String filename = "E:/456.txt";
		int threadsize = 3;
		try {
			assertTrue(rbf.readBigFile(filename, threadsize,1004));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

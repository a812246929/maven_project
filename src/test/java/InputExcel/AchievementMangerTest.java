package InputExcel;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class AchievementMangerTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(AchievementMangerTest.class.getName());

	static AchievementManger achievementManger;

	@BeforeAll
	public static void Before() {
		System.out.println(1);
		achievementManger = new AchievementMangerImpl();
		String student_data_file = "/work.xlsx";
		achievementManger.load(student_data_file);
	}

	@Test
	public void testFindByFullname() {
		String name = "李瑞林";
		Achievement achievement = achievementManger.findByFullname(name);
		if (logger.isDebugEnabled()) {
			logger.error(achievement); //$NON-NLS-1$
		} 
	}

	@Test
	public void testFindByPostcode() {
		String code = "201608030325";
		Achievement achievement = achievementManger.findByPostcode(code);
		if (logger.isDebugEnabled()) {
			logger.error(achievement); //$NON-NLS-1$
		} 
	}
	
//	@Test
//	public void load() {
//		String student_data_file = "/work.xlsx";
//		this.achievementManger.load(student_data_file);
//	}
}

package dao.exceldao.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import InputExcel.Achievement;
import dao.exceldao.ExcelDao;

class ExcelDaoImplTest {
	ExcelDao excel = new ExcelDaoImpl();
	@Test
	void testImportMySQL() {
		//由excel导入到Mysql
		String student_data_file = "/work.xlsx";
		excel.importMySQL(student_data_file);
	}

	@Test
	void testOutExcel() {
		//导出excel到list列表中
		String student_data_file = "/work.xlsx";
		List<Achievement> list = new ArrayList();
		list = excel.outExcel(student_data_file);
		System.out.println(list);
	}
	
	@Test
	void testExportJson() {
		String json_file = "E:\\exceltest.json";
		String data_file = "/work.xlsx";
		excel.outExcel(data_file,json_file);
	}
	
	@Test
	void testJsonToExcel() {
		String json_file = "E:\\exceltest.json";
		String data_file = "/work.xlsx";
		excel.jsonToExcel(json_file, data_file);
	}
	
}

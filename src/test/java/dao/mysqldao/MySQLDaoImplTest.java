package dao.mysqldao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import InputExcel.Achievement;
import dao.exceldao.ExcelDao;
import dao.mysqldao.impl.MySQLDaoImpl;

class MySQLDaoImplTest {
	
	private static final Logger logger = LogManager.getLogger(MySQLDaoImplTest.class.getName());
	//数据库操作对象
	MySQLDao<Achievement> mysql = null;
	
	@BeforeEach
	void before() {
		this.mysql = new MySQLDaoImpl();
	}

	@Test
	//生成CSV文件
	void testImportExcel() {
		mysql.importExcel("E:\\test.csv");
	}
	/**
	 * Logger for this class
	 */

	@Test
  //由excel导入到mysql 
	void testExcelToMySQL() {
		String student_data_file = "/work.xlsx";
		mysql.excelToMySQL(student_data_file);
	}
	
	@Test
	//查找名字
	void testFindByName() {
		List <Achievement> list = new ArrayList<>();
		String name = "李瑞林";
		list = mysql.findByName(name);
		for (Achievement achievement :list ){
			if(achievement.getName().equals(name))
				if (logger.isDebugEnabled()) {
					logger.debug(achievement); //$NON-NLS-1$
				}
		}
	}

	@Test
	//更新mysql
	void testUpdateMySQL() {
		Achievement achievement = new Achievement();
		achievement.setId(12l);
		achievement.setName("杨瑞龙");
		mysql.updateMySQL(achievement);
	}

	@Test
	//删除mysql
	void testRemoveMySQL() {
		String name = "李瑞林";
		int i = 0;
		i = mysql.removeMySQL(name);
		if(i==1)
			System.out.println("删除成功");
		else
			System.out.println("删除失败");
	}

	@Test
	//查找ID
	void testFindByID() {
		Long id = Long.valueOf(5);
		Achievement achievement = new Achievement();
		achievement = this.mysql.findByID(id);
		assertNotNull(achievement);
		assertEquals(id, achievement.getId());
		if (logger.isDebugEnabled()) {
			logger.debug(achievement); //$NON-NLS-1$
		}
	}
	
	@Test
	void testCreatMySQL() {
		Achievement achievement = new Achievement();
		achievement.setId(12l);
		achievement.setName("杨瑞龙");
		mysql.creatMySQL(achievement);
	}
	@Test
	void testMySQLToJson(){
		String File_Data = "E:\\test.json";
		mysql.mysqlTotJson(File_Data);
		
	}
	
	@Test
	void testJsonToMySQL() {
		String File_Data = "E:\\test.json";
		mysql.jsonToMySQL(File_Data);
	}
	
	@Test
	void testCsvToMySQL() {
		String File_Data = "E:\\test.csv";
		mysql.csvToMySQL(File_Data);
	}

}

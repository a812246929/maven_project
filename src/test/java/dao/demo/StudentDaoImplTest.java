package dao.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import basedemo.Student;

class StudentDaoImplTest {
	List<Student> students = new ArrayList<>();
	StudentDao s = new StudentDaoImpl();
	@Test
	void testFindByName() {
		students = s.findByName("li");
		for(Student st : students) {
			System.out.println(st);
		}
	}
}

package dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import InputExcel.Achievement;
import dao.demo.ExcelToMySQL;
import dao.demo.ExcelToMySQLImpl;
import dao.demo.StudentDao;
import dao.demo.StudentDaoImpl;

class ExcelToMySQLImplTest1 {
	ExcelToMySQL excel = new ExcelToMySQLImpl();
	StudentDao s = new StudentDaoImpl();
	List<Achievement> list = new ArrayList<>();
	@Test
	void test() {
		String student_data_file = "/work.xlsx";
		list = s.outExcel(student_data_file);
		excel.importMySQL(list);
	}

}

package dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import dao.demo.ExcelToMySQL;
import dao.demo.ExcelToMySQLImpl;


class ExcelToMySQLImplTest {
	ExcelToMySQL excel = new ExcelToMySQLImpl();
	
	@Test
	void testMySQL() {
		String student_data_file = "/work.xlsx";
		excel.MySQL(student_data_file);	
	}

}
